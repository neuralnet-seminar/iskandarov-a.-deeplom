package ru.mechmat.azzgzz.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import ru.mechmat.azzgzz.db.DbCreator;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.repository.impl.IncomingRepositoryImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.stream.IntStream;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TickerRepositoryTest {

    @Autowired
    IncomingRepositoryImpl tickersRepository;

    private final Date testTime = new Date();

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Before
    public void createTableInDatabase() {
        DbCreator.createDb(jdbcTemplate);
    }

    @After
    public void clearTable() {
        jdbcTemplate.execute("delete from tickers");
    }

    @Test
    public void testInsert() {
        Assert.assertEquals(IntStream.range(0, 60)
                .mapToObj(i -> new NumericCandlestick()
                        .setOpenTime(testTime.getTime()-(60-i)*1000)
                        .setOpen(BigDecimal.valueOf(i))
                        .setHigh(BigDecimal.valueOf(i*2))
                        .setLow(BigDecimal.valueOf(i/2.))
                        .setClose(BigDecimal.valueOf(i))
                        .setCloseTime(testTime.getTime()-(59-i)*1000)
                        .setVolume(BigDecimal.valueOf(10 + i*i - i*3))
                        .setQuoteAssetVolume(BigDecimal.valueOf(i))
                        .setNumberOfTrades(BigDecimal.valueOf(i*i))
                        .setTakerBuyBaseAssetVolume("buy base " + i)
                        .setTakerBuyQuoteAssetVolume("buy quote " + i)
                        .setTicker(Ticker.ETHUSDT)
                ).map(x -> tickersRepository.insertCandlestick(x))
                .mapToInt(x -> x)
                .sum(),
                60);
    }

    @Test
    public void testSelectAll() {
        clearTable();
        testInsert();
        Assert.assertEquals(tickersRepository.getAllCandlesticks()
                .stream()
                .map(NumericCandlestick::getClose)
                .reduce(BigDecimal.ZERO, BigDecimal::add).longValue(),
                1770);
    }

    @Test
    public void testSelectSingle() {
        testInsert();
        Assert.assertNotNull(tickersRepository.get60CandlesticksFromTime(Ticker.ETHUSDT, testTime.getTime() - 2000));
    }
}
