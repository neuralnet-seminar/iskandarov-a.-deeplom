package ru.mechmat.azzgzz.util;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.junit.Assert.*;

public class TimeUtilsTest {

    @Test(expected = AssertionError.class)
    public void testAssert() {
        TimeUtils.truncate(11234L, ChronoUnit.DAYS);
    }

    @Test
    public void testSeconds() {
        assertEquals(61000L, (long) TimeUtils.truncate(61234L));
    }

    @Test
    public void testMinutes() {
        assertEquals(60000L, (long) TimeUtils.truncate(61234L, ChronoUnit.MINUTES));
    }

    @Test
    public void timeTest() {
        Date o = new Date(1576411028028L);
        System.out.println(o);
        assertEquals(1576411028028L, o.getTime());
    }
}