package ru.mechmat.azzgzz.singleton;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import ru.mechmat.azzgzz.db.DbCreator;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.repository.IncomingRepository;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ru.mechmat.azzgzz.repository.sql.InsertCandlestickQuery.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CsvReaderTest {

    private Reader reader;

    private CSVParser csvParser;

    @Autowired
    JdbcTemplate jdbcTemplate;

    public CsvReaderTest() throws IOException {
    }

    public enum Headers{
        DATE, TIME, OPEN, HIGH, LOW, CLOSE, VOL;

        @Override
        public String toString() {
            return "<" + this.name() + ">";
        }
    }


    @Before
    public void createTableInDatabase() {
        DbCreator.createDb(jdbcTemplate);
    }

    @After
    public void clearTable() {
        jdbcTemplate.execute("delete from " + TICKERS_TABLE);
    }


    @Before
    public void openCsv () throws IOException {
        reader = Files.newBufferedReader(Paths.get("src/python/main/Second/market_data/eth-usd-1min.csv"));
        csvParser = CSVParser.parse(reader, CSVFormat.DEFAULT
                .withFirstRecordAsHeader());
    }

    @After
    public void closeCsv() throws IOException {
        csvParser.close();
        reader.close();
    }
    @Test
    public void test() {
        Iterator<CSVRecord> iterator = csvParser.iterator();
        for (int i = 0; i < 7; i++) {
            CSVRecord line = iterator.next();
            System.out.println(line);
        }
    }

    private Date parseDateTime(CSVRecord record) throws ParseException {
        return new SimpleDateFormat("dd/MM/yy hh:mm")
                .parse(record.get(Headers.DATE) + " " + record.get(Headers.TIME));
    }

    @Autowired
    IncomingRepository incomingRepository;

    @Test
    public void testAlgo() throws ParseException {
        Iterator<CSVRecord> iterator = csvParser.iterator();
        CSVRecord previousRecord = iterator.next();
        incomingRepository.insertCandlestick(mapTo(previousRecord));
        int i = 0;
        while (iterator.hasNext()) {
            CSVRecord currentRecord = iterator.next();
            incomingRepository.insertCandlestick(generate(previousRecord, currentRecord));
            i++;
            if (i > 30)
                break;
            previousRecord = currentRecord;
        }
    }

    private NumericCandlestick mapTo(CSVRecord record) throws ParseException {
        long time = parseDateTime(record).getTime();
        return new NumericCandlestick()
                .setOpenTime(time)
                .setOpen(new BigDecimal(record.get(Headers.OPEN)))
                .setHigh(new BigDecimal(record.get(Headers.HIGH)))
                .setLow(new BigDecimal(record.get(Headers.LOW)))
                .setClose(new BigDecimal(record.get(Headers.CLOSE)))
                .setCloseTime(time + 60_000)
                .setVolume(new BigDecimal(record.get(Headers.VOL)))
                .setTicker(Ticker.ETHUSDT);
    }

    private List<NumericCandlestick> generate(CSVRecord fromExclusive, CSVRecord toInclusive) throws ParseException {
        long prevDate = parseDateTime(fromExclusive).getTime();
        long curDate = parseDateTime(toInclusive).getTime();
        List<NumericCandlestick> generatedList = IntStream.range(1, getGeneratedCount(prevDate, curDate))
                .mapToObj(i -> {
                            BigDecimal constantPrice = new BigDecimal(fromExclusive.get(Headers.CLOSE));
                            return new NumericCandlestick()
                                    .setOpenTime(prevDate + i * 60_000)
                                    .setOpen(constantPrice)
                                    .setHigh(constantPrice)
                                    .setLow(constantPrice)
                                    .setClose(constantPrice)
                                    .setCloseTime(prevDate + (i + 1) * 60_000)
                                    .setVolume(BigDecimal.ZERO)
                                    .setTicker(Ticker.ETHUSDT);
                        }
                ).collect(Collectors.toList());
        generatedList.add(mapTo(toInclusive));
        return generatedList;
    }

    private int getGeneratedCount(long a, long b) {
        return ((int) (b-a)/1000)/60;
    }

}
