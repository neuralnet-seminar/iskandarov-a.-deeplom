package ru.mechmat.azzgzz.db;

import org.springframework.jdbc.core.JdbcTemplate;

import static ru.mechmat.azzgzz.repository.sql.InsertCandlestickQuery.*;

public class DbCreator {

    public static void createDb(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.execute("create table if not exists " + TICKERS_TABLE+ "(" +
                OPEN_TIME + " numeric," +
                OPEN + " numeric," +
                HIGH + " numeric," +
                LOW + " numeric," +
                CLOSE + " numeric," +
                CLOSE_TIME + " numeric," +
                VOLUME + " numeric," +
                QUOTE_ASSET_VOLUME + " numeric," +
                NUMBER_OF_TRADES + " numeric," +
                TAKER_BUY_BASE_ASSET_VOLUME + " varchar," +
                TAKER_BUY_QUOTE_ASSET_VOLUME + " varchar, " +
                TICKER + " int" +
                ")");
    }
}
