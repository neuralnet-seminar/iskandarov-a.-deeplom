package ru.mechmat.azzgzz.logic;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.OrderSide;
import com.binance.api.client.domain.OrderStatus;
import com.binance.api.client.domain.OrderType;
import com.binance.api.client.domain.TimeInForce;
import com.binance.api.client.domain.account.NewOrder;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import com.binance.api.client.domain.market.TickerPrice;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.client.BinanceMock;
import ru.mechmat.azzgzz.service.IncomingService;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class BinanceMockTest {


    BinanceApiRestClient binanceApiRestClient;

    @Before
    public void prepare() {
        IncomingService incomingService = Mockito.mock(IncomingService.class);
        Mockito.when(incomingService.get60CandlesticksFromTime(Mockito.any(), Mockito.anyLong()))
                .thenReturn(IntStream.range(0, 60).mapToObj(i -> {
                    Candlestick candlestick = new Candlestick();
                    candlestick.setOpenTime(0L);
                    candlestick.setCloseTime(0L);
                    candlestick.setClose("50");
                    candlestick.setOpen("3");
                    candlestick.setHigh("43");
                    candlestick.setLow("32");
                    candlestick.setVolume("33");
                    candlestick.setQuoteAssetVolume("33");
                    candlestick.setNumberOfTrades(3L);
                    return candlestick;
                        }).collect(Collectors.toList())
                );
        binanceApiRestClient = Mockito.mock(BinanceMock.class);
        binanceApiRestClient.getCandlestickBars(Ticker.ETHUSDT.name(), CandlestickInterval.DAILY);
    }

    @Test
    public void testBuy() {
        TickerPrice tickerPrice = new TickerPrice();
        tickerPrice.setPrice("50");
        NewOrder newOrder = new NewOrder("ETHUSDT", OrderSide.BUY, OrderType.MARKET, TimeInForce.GTC, "1");
        Assert.assertEquals("1", binanceApiRestClient.newOrder(newOrder).getExecutedQty());
        Assert.assertEquals("49.37375000", binanceApiRestClient.getAccount().getBalances().get(0).getFree());
    }

    @Test
    public void testSell() {
        TickerPrice tickerPrice = new TickerPrice();
        tickerPrice.setPrice("50");
        NewOrder newOrder = new NewOrder("ETHUSDT", OrderSide.SELL, OrderType.MARKET, TimeInForce.GTC, "1");
        Assert.assertEquals(OrderStatus.REJECTED, binanceApiRestClient.newOrder(newOrder).getStatus());

        testBuy();
        Assert.assertEquals("1", binanceApiRestClient.newOrder(newOrder).getExecutedQty());
        Assert.assertEquals("98.75000000", binanceApiRestClient.getAccount().getBalances().get(0).getFree());
    }
}
