package ru.mechmat.azzgzz.dictionary;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TickerTest {

    @Test
    public void testOrdinals() {
        assertEquals(0, Ticker.UNKNOWN.ordinal());
        assertEquals(1, Ticker.BTCUSDT.ordinal());
        assertEquals(2, Ticker.ETHUSDT.ordinal());
    }

}