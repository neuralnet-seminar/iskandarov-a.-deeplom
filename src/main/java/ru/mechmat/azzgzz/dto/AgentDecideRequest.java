package ru.mechmat.azzgzz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AgentDecideRequest {

    private List<List<BigDecimal>> priceDeltaLastHour;

    private BigDecimal coinPart;

    private BigDecimal coinHourDelta;

    private BigDecimal portfolioHourDelta;

    @JsonProperty("coin_d_delta")
    private BigDecimal coinDDelta = BigDecimal.ZERO;

    @JsonProperty("coin_w_delta")
    private BigDecimal coinWDelta = BigDecimal.ZERO;

    @JsonProperty("coin_m_delta")
    private BigDecimal coinMDelta = BigDecimal.ZERO;

    @JsonProperty("coin_3m_delta")
    private BigDecimal coin3MDelta = BigDecimal.ZERO;

    @JsonProperty("coin_6m_delta")
    private BigDecimal coin6MDelta = BigDecimal.ZERO;

    @JsonProperty("coin_y_delta")
    private BigDecimal coinYDelta = BigDecimal.ZERO;

    @JsonProperty("portfolio_d_delta")
    private BigDecimal portfolioDDelta = BigDecimal.ZERO;

    @JsonProperty("portfolio_w_delta")
    private BigDecimal portfolioWDelta = BigDecimal.ZERO;

    @JsonProperty("portfolio_m_delta")
    private BigDecimal portfolioMDelta = BigDecimal.ZERO;

    private BigDecimal deviationExp = BigDecimal.ZERO;

    private BigDecimal deviationLine = BigDecimal.ZERO;

    private BigDecimal volatilityExp = BigDecimal.ZERO;

    private BigDecimal volatilityLine = BigDecimal.ZERO;

    @JsonProperty("m_drowdown")
    private BigDecimal mDrowdown = BigDecimal.ZERO;
}
