package ru.mechmat.azzgzz.converter;

import com.binance.api.client.domain.account.AssetBalance;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;

import java.math.BigDecimal;
import java.math.RoundingMode;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AssetBalanceConverter {

    public static AssetBalance convert(NumericAssetBalance numericAssetBalance) {
        AssetBalance assetBalance = new AssetBalance();
        assetBalance.setAsset(numericAssetBalance.getAsset());
        assetBalance.setFree(numericAssetBalance.getFree().setScale(8, RoundingMode.HALF_UP).toString());
        assetBalance.setLocked(numericAssetBalance.getLocked().setScale(8, RoundingMode.HALF_UP).toString());
        return assetBalance;
    }

    public static NumericAssetBalance convert(AssetBalance assetBalance, Long time) {
        return new NumericAssetBalance()
                .setAsset(assetBalance.getAsset())
                .setFree(new BigDecimal(assetBalance.getFree()))
                .setLocked(new BigDecimal(assetBalance.getLocked()))
                .setTime(time);
    }
}
