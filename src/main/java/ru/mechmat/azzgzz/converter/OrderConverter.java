package ru.mechmat.azzgzz.converter;

import com.binance.api.client.domain.account.NewOrder;
import com.binance.api.client.domain.account.NewOrderResponseType;
import ru.mechmat.azzgzz.entity.market.NumericNewOrder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

public class OrderConverter {

    public static NewOrder newOrder(NumericNewOrder order) {
        return new NewOrder(
                order.getTicker().name(),
                order.getSide(),
                order.getType(),
                order.getTimeInForce(),
                order.getQuantity().setScale(8, RoundingMode.HALF_UP).toString()
//                order.getPrice().setScale(8, RoundingMode.HALF_UP).toString()
        );
    }

    public static BigDecimal getNullable(String stringValue) {
        return Optional.ofNullable(stringValue).map(BigDecimal::new).orElse(null);
    }
}
