package ru.mechmat.azzgzz.converter;

import com.binance.api.client.domain.market.Candlestick;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;

import java.math.BigDecimal;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CandlestickConverter {

    public static Candlestick convert(NumericCandlestick numericCandlestick) {
        Candlestick candlestick = new Candlestick();
        candlestick.setOpenTime(numericCandlestick.getOpenTime());
        candlestick.setOpen(String.valueOf(numericCandlestick.getOpen()));
        candlestick.setHigh(String.valueOf(numericCandlestick.getHigh()));
        candlestick.setLow(String.valueOf(numericCandlestick.getLow()));
        candlestick.setClose(String.valueOf(numericCandlestick.getClose()));
        candlestick.setVolume(String.valueOf(numericCandlestick.getVolume()));
        candlestick.setCloseTime(numericCandlestick.getCloseTime());
        candlestick.setQuoteAssetVolume(Optional.ofNullable(numericCandlestick.getQuoteAssetVolume()).map(BigDecimal::toString).orElse(null));
        candlestick.setNumberOfTrades(Optional.ofNullable(numericCandlestick.getNumberOfTrades()).map(BigDecimal::longValue).orElse(null));
        candlestick.setTakerBuyBaseAssetVolume(numericCandlestick.getTakerBuyBaseAssetVolume());
        candlestick.setTakerBuyQuoteAssetVolume(numericCandlestick.getTakerBuyQuoteAssetVolume());

        return candlestick;
    }

    public static NumericCandlestick convert(Candlestick candlestick, Ticker ticker) {
        return new NumericCandlestick()
                .setOpenTime(candlestick.getOpenTime())
                .setOpen(Optional.ofNullable(candlestick.getOpen()).map(BigDecimal::new).orElse(null))
                .setHigh(Optional.ofNullable(candlestick.getHigh()).map(BigDecimal::new).orElse(null))
                .setLow(Optional.ofNullable(candlestick.getLow()).map(BigDecimal::new).orElse(null))
                .setClose(Optional.ofNullable(candlestick.getClose()).map(BigDecimal::new).orElse(null))
                .setVolume(Optional.ofNullable(candlestick.getVolume()).map(BigDecimal::new).orElse(null))
                .setCloseTime(candlestick.getCloseTime())
                .setQuoteAssetVolume(Optional.ofNullable(candlestick.getQuoteAssetVolume()).map(BigDecimal::new).orElse(null))
                .setNumberOfTrades(Optional.ofNullable(candlestick.getNumberOfTrades()).map(BigDecimal::new).orElse(null))
                .setTakerBuyBaseAssetVolume(candlestick.getTakerBuyBaseAssetVolume())
                .setTakerBuyQuoteAssetVolume(candlestick.getTakerBuyQuoteAssetVolume())
                .setTicker(ticker);
    }
}
