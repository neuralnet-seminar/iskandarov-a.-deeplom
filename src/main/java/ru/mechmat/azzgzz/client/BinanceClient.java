package ru.mechmat.azzgzz.client;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "api.binance.mode", havingValue = "real")
public class BinanceClient {

    @Bean
    public BinanceApiRestClient binanceApiRestClient(){
        BinanceApiRestClient binanceApiRestClient = BinanceApiClientFactory.newInstance("key", "secret").newRestClient();
        return binanceApiRestClient;
    }
}
