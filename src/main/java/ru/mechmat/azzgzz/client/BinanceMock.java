package ru.mechmat.azzgzz.client;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.OrderSide;
import com.binance.api.client.domain.OrderStatus;
import com.binance.api.client.domain.OrderType;
import com.binance.api.client.domain.account.*;
import com.binance.api.client.domain.account.request.*;
import com.binance.api.client.domain.general.Asset;
import com.binance.api.client.domain.general.ExchangeInfo;
import com.binance.api.client.domain.market.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import ru.mechmat.azzgzz.converter.AssetBalanceConverter;
import ru.mechmat.azzgzz.converter.CandlestickConverter;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.repository.learning.AgentLearningAccountRepository;
import ru.mechmat.azzgzz.service.IncomingService;
import ru.mechmat.azzgzz.util.AccountUtils;

import javax.xml.ws.Holder;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static ru.mechmat.azzgzz.service.TradingProcessService.HOUR_TIME_STEP;

@Slf4j
@RequiredArgsConstructor
@Component
@ConditionalOnProperty(value = "api.binance.mode", havingValue = "mock")
public class BinanceMock implements BinanceApiRestClient {

    private final IncomingService incomingService;

    private final AgentLearningAccountRepository agentLearningAccountRepository;

    private Holder<NumericCandlestick> lastCandlestick = new Holder<>();

    private final BigDecimal commission = BigDecimal.valueOf(0.0025).setScale(8, BigDecimal.ROUND_HALF_UP);
    private List<NumericAssetBalance> currentNumericAccount;

    @Override
    public void ping() {
        log.info(":pinged:");
    }

    @Override
    public Long getServerTime() {
        return agentLearningAccountRepository.getCurrentTime() + HOUR_TIME_STEP;
    }

    @Override
    public ExchangeInfo getExchangeInfo() {
        ExchangeInfo exchangeInfo = new ExchangeInfo();
        return exchangeInfo;
    }

    @Override
    public List<Asset> getAllAssets() {
        return null;
    }

    @Override
    public OrderBook getOrderBook(String symbol, Integer limit) {
        return null;
    }

    @Override
    public List<TradeHistoryItem> getTrades(String symbol, Integer limit) {
        return null;
    }

    @Override
    public List<TradeHistoryItem> getHistoricalTrades(String symbol, Integer limit, Long fromId) {
        return null;
    }

    @Override
    public List<AggTrade> getAggTrades(String symbol, String fromId, Integer limit, Long startTime, Long endTime) {
        return null;
    }

    @Override
    public List<AggTrade> getAggTrades(String symbol) {
        return null;
    }

    @Override
    public List<Candlestick> getCandlestickBars(String symbol, CandlestickInterval interval, Integer limit, Long startTime, Long endTime) {
        List<Candlestick> candlesticksFromTime = incomingService.get60CandlesticksFromTime(Ticker.fromValue(symbol), startTime);
        lastCandlestick.value = CandlestickConverter.convert(candlesticksFromTime.get(59), Ticker.valueOf(symbol));
        return candlesticksFromTime;
    }

    @Override
    @Deprecated
    public List<Candlestick> getCandlestickBars(String symbol, CandlestickInterval interval) {
        //return getCandlestickBars(symbol, interval, 60, new Date().getTime() - 60 * 1000, new Date().getTime());
        throw new UnsupportedOperationException("You should never call this function");
    }

    @Override
    public TickerStatistics get24HrPriceStatistics(String symbol) {
        return null;
    }

    @Override
    public List<TickerStatistics> getAll24HrPriceStatistics() {
        return null;
    }

    @Override
    public List<TickerPrice> getAllPrices() {
        return null;
    }

    @Override
    public TickerPrice getPrice(String symbol) {
        TickerPrice tickerPrice = new TickerPrice();
        tickerPrice.setSymbol(symbol);
        tickerPrice.setPrice(lastCandlestick.value.getClose().toString());
        return tickerPrice;
    }

    @Override
    public List<BookTicker> getBookTickers() {
        return null;
    }

    @Override
    // бот использует только market запросы
    public NewOrderResponse newOrder(NewOrder order) {
        getAccount();
        Ticker ticker = Ticker.valueOf(order.getSymbol());
        BigDecimal assetQty = AccountUtils.getAssetQty(currentNumericAccount, ticker.getBaseAsset());
        BigDecimal cash = AccountUtils.getAssetQty(currentNumericAccount, ticker.getQuoteAsset());

        switch (order.getSide()) {
            case BUY: {
                return buyOrder(order, assetQty, cash);
            }
            case SELL: {
                return sellOrder(order, assetQty, cash);
            }
            default:
                throw new RuntimeException("Unknown order side");
        }
    }

    private NewOrderResponse buyOrder(NewOrder order, BigDecimal assetQty, BigDecimal cash) {
        BigDecimal price = new BigDecimal(getPrice(order.getSymbol()).getPrice());
        BigDecimal volume = price.multiply(new BigDecimal(order.getQuantity()));
        BigDecimal volumeWithCommission = volume.add(getCommission(volume)).multiply(BigDecimal.valueOf(1.0015));// провал в стакан
        if (volumeWithCommission.compareTo(cash) > 0) {
            return fillNewOrderResponse(order, order.getSide(), OrderStatus.REJECTED);
        }
        cash = cash.subtract(volumeWithCommission);
        assetQty = assetQty.add(new BigDecimal(order.getQuantity()));
        return addRowToLearningAccount(order, assetQty, cash);
    }

    private NewOrderResponse sellOrder(NewOrder order, BigDecimal assetQty, BigDecimal cash) {
        BigDecimal price = new BigDecimal(getPrice(order.getSymbol()).getPrice());
        BigDecimal volume = price.multiply(new BigDecimal(order.getQuantity()));
        BigDecimal volumeWithCommission = volume.subtract(getCommission(volume)).multiply(BigDecimal.valueOf(0.9985));
        if (new BigDecimal(order.getQuantity()).compareTo(assetQty) > 0) {
            return fillNewOrderResponse(order, order.getSide(), OrderStatus.REJECTED);
        }
        cash = cash.add(volumeWithCommission);
        assetQty = assetQty.subtract(new BigDecimal(order.getQuantity()));
        return addRowToLearningAccount(order, assetQty, cash);
    }

    private NewOrderResponse addRowToLearningAccount(NewOrder order, BigDecimal assetQty, BigDecimal cash) {
        agentLearningAccountRepository.insertAccount(Arrays.asList(
                new NumericAssetBalance()
                        .setTime(getServerTime())
                        .setAgentId(currentNumericAccount.get(0).getAgentId())
                        .setAsset(Ticker.valueOf(order.getSymbol()).getBaseAsset())
                        .setFree(assetQty.setScale(8, RoundingMode.HALF_UP)),
                new NumericAssetBalance()
                        .setTime(getServerTime())
                        .setAgentId(currentNumericAccount.get(0).getAgentId())
                        .setAsset(Ticker.valueOf(order.getSymbol()).getQuoteAsset())
                        .setFree(cash.setScale(8, RoundingMode.HALF_UP))
        ));
        return fillNewOrderResponse(order, order.getSide(), OrderStatus.FILLED);
    }

    private BigDecimal getCommission(BigDecimal volume) {
        return commission.multiply(volume);
    }

    // mock status may be FILLED or REJECTED
    private NewOrderResponse fillNewOrderResponse(NewOrder order, OrderSide side, OrderStatus status) {
        NewOrderResponse newOrderResponse = new NewOrderResponse();
        newOrderResponse.setSymbol(order.getSymbol());
        newOrderResponse.setTransactTime(order.getTimestamp() + 50);
        newOrderResponse.setPrice(order.getPrice());
        newOrderResponse.setOrigQty(order.getQuantity());
        newOrderResponse.setExecutedQty(status == OrderStatus.FILLED ? order.getQuantity() : "0");
        newOrderResponse.setStatus(status);
        newOrderResponse.setTimeInForce(order.getTimeInForce());
        newOrderResponse.setType(OrderType.MARKET);
        newOrderResponse.setSide(side);
        return newOrderResponse;
    }

    @Override
    public void newOrderTest(NewOrder order) {

    }

    @Override
    public Order getOrderStatus(OrderStatusRequest orderStatusRequest) {
        return null;
    }

    @Override
    public CancelOrderResponse cancelOrder(CancelOrderRequest cancelOrderRequest) {
        return null;
    }

    @Override
    public List<Order> getOpenOrders(OrderRequest orderRequest) {
        return null;
    }

    @Override
    public List<Order> getAllOrders(AllOrdersRequest orderRequest) {
        return null;
    }

    @Override
    public Account getAccount(Long recvWindow, Long timestamp) {
        return null;
    }

    @Override
    public Account getAccount() {
        Account account = new Account();
        currentNumericAccount = agentLearningAccountRepository.getAccount(getServerTime() - HOUR_TIME_STEP);
        List<AssetBalance> assetBalances = currentNumericAccount
                .stream()
                .map(AssetBalanceConverter::convert)
                .collect(Collectors.toList());
        account.setBalances(assetBalances);
        return account;
    }

    @Override
    public List<Trade> getMyTrades(String symbol, Integer limit, Long fromId, Long recvWindow, Long timestamp) {
        return null;
    }

    @Override
    public List<Trade> getMyTrades(String symbol, Integer limit) {
        return null;
    }

    @Override
    public List<Trade> getMyTrades(String symbol) {
        return null;
    }

    @Override
    public WithdrawResult withdraw(String asset, String address, String amount, String name, String addressTag) {
        return null;
    }

    @Override
    public DepositHistory getDepositHistory(String asset) {
        return null;
    }

    @Override
    public WithdrawHistory getWithdrawHistory(String asset) {
        return null;
    }

    @Override
    public DepositAddress getDepositAddress(String asset) {
        return null;
    }

    @Override
    public String startUserDataStream() {
        return null;
    }

    @Override
    public void keepAliveUserDataStream(String listenKey) {

    }

    @Override
    public void closeUserDataStream(String listenKey) {

    }
}
