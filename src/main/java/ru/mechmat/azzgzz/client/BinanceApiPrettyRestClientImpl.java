package ru.mechmat.azzgzz.client;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.account.*;
import com.binance.api.client.domain.account.request.*;
import com.binance.api.client.domain.general.Asset;
import com.binance.api.client.domain.general.ExchangeInfo;
import com.binance.api.client.domain.market.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;
import ru.mechmat.azzgzz.converter.OrderConverter;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericAccount;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.entity.market.NumericNewOrder;
import ru.mechmat.azzgzz.entity.market.NumericNewOrderResponse;
import ru.mechmat.azzgzz.converter.CandlestickConverter;

import java.util.List;
import java.util.stream.Collectors;

import static ru.mechmat.azzgzz.service.TradingProcessService.HOUR_TIME_STEP;

@Slf4j
@Component
@RequiredArgsConstructor
public class BinanceApiPrettyRestClientImpl implements BinanceApiPrettyRestClient {

    private final BinanceApiRestClient binanceApiRestClient;

    @Override
    public void ping() {
        binanceApiRestClient.ping();
    }

    @Override
    public Long getServerTime() {
        return binanceApiRestClient.getServerTime();
    }

    @Override
    public ExchangeInfo getExchangeInfo() {
        return binanceApiRestClient.getExchangeInfo();
    }

    @Override
    public List<Asset> getAllAssets() {
        return binanceApiRestClient.getAllAssets();
    }

    @Override
    public OrderBook getOrderBook(String symbol, Integer limit) {
//        return binanceApiRestClient.getOrderBook(symbol, limit);
        throw new NotImplementedException("Стакана заявок не используется");
    }

    @Override
    public List<TradeHistoryItem> getTrades(String symbol, Integer limit) {
//        return binanceApiRestClient.getTrades(symbol, limit);
        throw new NotImplementedException("Чужие последние сделки не интересны");
    }

    @Override
    public List<TradeHistoryItem> getHistoricalTrades(String symbol, Integer limit, Long fromId) {
//        return binanceApiRestClient.getHistoricalTrades(symbol, limit, fromId);
        throw new NotImplementedException("История чужих сделок не важна");
    }

    @Override
    public List<AggTrade> getAggTrades(String symbol, String fromId, Integer limit, Long startTime, Long endTime) {
//        return binanceApiRestClient.getAggTrades(symbol, fromId, limit, startTime, endTime);
        throw new NotImplementedException("Аггрегированные сделки не интересны");
    }

    @Override
    public List<AggTrade> getAggTrades(String symbol) {
//        return binanceApiRestClient.getAggTrades(symbol);
        throw new NotImplementedException("Аггрегированные сделки не интересны");
    }

    @Override
    public List<NumericCandlestick> getCandlestickBars(Ticker ticker, CandlestickInterval interval, Integer limit, Long startTime, Long endTime) {
        return binanceApiRestClient.getCandlestickBars(ticker.name(), interval, limit, startTime, endTime)
                .stream()
                .map(x -> CandlestickConverter.convert(x, ticker))
                .collect(Collectors.toList());
    }

    @Override
        public List<NumericCandlestick> get60PreviousMinutesBars(Ticker ticker, Long currentTruncatedTime) {
        return getCandlestickBars(ticker, CandlestickInterval.ONE_MINUTE, 60, currentTruncatedTime - HOUR_TIME_STEP, currentTruncatedTime);
    }

    @Override
    public TickerStatistics get24HrPriceStatistics(String symbol) {
//        return binanceApiRestClient.get24HrPriceStatistics(symbol);
        throw new NotImplementedException("Статистика за 24 часа не интересна");
    }

    @Override
    public List<TickerStatistics> getAll24HrPriceStatistics() {
//        return binanceApiRestClient.getAll24HrPriceStatistics();
        throw new NotImplementedException("Статистика за 24 часа не интересна");
    }

    @Override
    public List<TickerPrice> getAllPrices() {
//        return binanceApiRestClient.getAllPrices();
        throw new NotImplementedException("Последняя цена не интересна");
    }

    @Override
    public TickerPrice getPrice(String symbol) {
//        return binanceApiRestClient.getPrice(symbol);
        throw new NotImplementedException("Последняя цена не интересна");
    }

    @Override
    public List<BookTicker> getBookTickers() {
//        return binanceApiRestClient.getBookTickers();
        throw new NotImplementedException("Самые лучшие цены не интересны");
    }

    @Override
    public NumericNewOrderResponse newOrder(NumericNewOrder order) {
        return new NumericNewOrderResponse(binanceApiRestClient.newOrder(OrderConverter.newOrder(order)));
    }

    @Override
    public void newOrderTest(NumericNewOrder order) {
        binanceApiRestClient.newOrderTest(OrderConverter.newOrder(order));
    }

    @Override
    public Order getOrderStatus(OrderStatusRequest orderStatusRequest) {
//        return binanceApiRestClient.getOrderStatus(orderStatusRequest);
        throw new NotImplementedException("Статус заявки не интересен");
    }

    @Override
    public CancelOrderResponse cancelOrder(CancelOrderRequest cancelOrderRequest) {
//        return binanceApiRestClient.cancelOrder(cancelOrderRequest);
        throw new NotImplementedException("Отмена заявки не интересна");
    }

    @Override
    public List<Order> getOpenOrders(OrderRequest orderRequest) {
//        return binanceApiRestClient.getOpenOrders(orderRequest);
        throw new NotImplementedException("Информация об открытых ордерах не интересна");
    }

    @Override
    public List<Order> getAllOrders(AllOrdersRequest orderRequest) {
//        return binanceApiRestClient.getAllOrders(orderRequest);
        throw new NotImplementedException("Информация об ордерах не интересна");
    }

    @Override
    public NumericAccount getAccount(Long recvWindow, Long timestamp) {
        return getAccount();
    }

    @Override
    public NumericAccount getAccount() {
        return new NumericAccount(binanceApiRestClient.getAccount());
    }

    @Override
    public List<Trade> getMyTrades(String symbol, Integer limit, Long fromId, Long recvWindow, Long timestamp) {
//        return null;
        throw new NotImplementedException("Информация об ордерах не интересна");
    }

    @Override
    public List<Trade> getMyTrades(String symbol, Integer limit) {
//        return null;
        throw new NotImplementedException("Информация об ордерах не интересна");
    }

    @Override
    public List<Trade> getMyTrades(String symbol) {
//        return null;
        throw new NotImplementedException("Информация об ордерах не интересна");
    }

    @Override
    public WithdrawResult withdraw(String asset, String address, String amount, String name, String addressTag) {
//        return null;
        throw new NotImplementedException("Вывод средств не интересен");
    }

    @Override
    public DepositHistory getDepositHistory(String asset) {
//        return null;
        throw new NotImplementedException("История пополнений не интересна");
    }

    @Override
    public WithdrawHistory getWithdrawHistory(String asset) {
//        return null;
        throw new NotImplementedException("История пополнений не интересна");
    }

    @Override
    public DepositAddress getDepositAddress(String asset) {
//        return null;
        throw new NotImplementedException("Пополнение депозита не интересно");
    }

    @Override
    public String startUserDataStream() {
//        return null;
        throw new NotImplementedException("Стримы не интересны");
    }

    @Override
    public void keepAliveUserDataStream(String listenKey) {
        throw new NotImplementedException("Стримы не интересны");
    }

    @Override
    public void closeUserDataStream(String listenKey) {
        throw new NotImplementedException("Стримы не интересны");
    }
}
