package ru.mechmat.azzgzz.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.mechmat.azzgzz.dto.AgentDecideRequest;
import ru.mechmat.azzgzz.dto.AgentLearnRequest;
import ru.mechmat.azzgzz.dto.AgentStatusResponse;

import java.math.BigDecimal;

@Slf4j
@Component
@RequiredArgsConstructor
public class ReinforcementAgent {

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    private static final String AGENT_URL = "http://localhost:5000/agent";
    private static final String INIT_URL = AGENT_URL + "/init";  // PUT
    private static final String RESET_URL = AGENT_URL + "/reset"; // PUT
    private static final String DECIDE_URL = AGENT_URL + "/decide"; // POST
    private static final String LEARN_URL = AGENT_URL + "/learn"; // POST
    private static final String SAVE_URL = AGENT_URL + "/save"; // GET
    private static final String LOAD_URL = AGENT_URL + "/load"; // GET

    public boolean init() {
        try {
            ResponseEntity<AgentStatusResponse> exchange =
                    restTemplate.exchange(INIT_URL, HttpMethod.PUT, null, AgentStatusResponse.class);
            return "created".equals(exchange.getBody().getStatus());
        } catch (RestClientException e) {
            log.error("Bad response from agent", e);
            return false;
        }
    }

    public boolean reset() {
        try {
            ResponseEntity<AgentStatusResponse> exchange =
                    restTemplate.exchange(RESET_URL, HttpMethod.PUT, null, AgentStatusResponse.class);
            return "reseted".equals(exchange.getBody().getStatus());
        } catch (RestClientException e) {
            log.error("Bad response from agent", e);
            return false;
        }
    }

    @SneakyThrows
    public BigDecimal decide(AgentDecideRequest request) {
        try {
            ResponseEntity<AgentStatusResponse> agentDecision =
                    restTemplate.postForEntity(DECIDE_URL, createHttpEntity(request), AgentStatusResponse.class);
            return agentDecision.getBody().getActionId();
        } catch (RestClientException e) {
            throw e;
        }
    }

    @SneakyThrows
    public boolean learn(AgentLearnRequest request) {
        try {
            ResponseEntity<AgentStatusResponse> learnResponse =
                    restTemplate.postForEntity(LEARN_URL, createHttpEntity(request), AgentStatusResponse.class);
            return true;
        } catch (RestClientException e) {
            return false;
        }
    }

    @SneakyThrows
    private <T> HttpEntity<String> createHttpEntity(T request) {
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<String> stringHttpEntity = new HttpEntity<>(objectMapper.writeValueAsString(request), headers);
        return stringHttpEntity;
    }
}
