package ru.mechmat.azzgzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeeplomApp {

    public static void main(String[] args) {
        SpringApplication.run(DeeplomApp.class);
    }
}
