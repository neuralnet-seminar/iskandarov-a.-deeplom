package ru.mechmat.azzgzz.dictionary;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum Ticker {
    UNKNOWN("NAN", "NAN"),
    BTCUSDT("BTC", "USDT"),
    ETHUSDT("ETH", "USDT");

    private final String baseAsset;
    private final String quoteAsset;

    public static Ticker fromValue(String text) {
        try {
            return Ticker.valueOf(text);
        } catch (IllegalArgumentException e) {
            return UNKNOWN;
        }
    }

    public static Ticker fromOrdinal(int ordinal) {
        return Stream.of(Ticker.values())
                .filter(x -> x.ordinal() == ordinal)
                .findAny()
                .orElseThrow(() -> new RuntimeException("Непонятный 'Ординал' у тикера: " + ordinal));
    }
}
