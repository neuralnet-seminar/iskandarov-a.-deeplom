package ru.mechmat.azzgzz.repository;

import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

public interface IncomingRepository {

    int insertCandlestick(@Valid NumericCandlestick numericCandlestick);

    List<NumericCandlestick> getAllCandlesticks();

    List<NumericCandlestick> get60CandlesticksFromTime(Ticker ticker, long openTime);

    int[] insertCandlestick(@Valid List<NumericCandlestick> candlestickList);
}
