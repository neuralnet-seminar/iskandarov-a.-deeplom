package ru.mechmat.azzgzz.repository.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.repository.IncomingRepository;
import ru.mechmat.azzgzz.repository.sql.InsertCandlestickQuery;
import ru.mechmat.azzgzz.repository.sql.SelectCandlestickQuery;

import javax.validation.Valid;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Validated
public class IncomingRepositoryImpl implements IncomingRepository {

    private final InsertCandlestickQuery insertCandlestickQuery;
    private final SelectCandlestickQuery selectCandlestickQuery;

    @Override
    public int insertCandlestick(@Valid NumericCandlestick numericCandlestick) {
        return insertCandlestickQuery.execute(numericCandlestick);
    }

    @Override
    @Deprecated
    public List<NumericCandlestick> getAllCandlesticks() {
        return selectCandlestickQuery.execute();
    }

    @Override
    public List<NumericCandlestick> get60CandlesticksFromTime(Ticker ticker, long openTime) {
        return selectCandlestickQuery.execute(ticker, openTime);
    }

    @Override
    public int[] insertCandlestick(@Valid List<NumericCandlestick> candlestickList) {
        return insertCandlestickQuery.execute(candlestickList);
    }


}
