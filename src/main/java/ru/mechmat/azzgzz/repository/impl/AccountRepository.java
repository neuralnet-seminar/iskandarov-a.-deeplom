package ru.mechmat.azzgzz.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;
import ru.mechmat.azzgzz.repository.sql.GetAccountQuery;
import ru.mechmat.azzgzz.repository.sql.InsertAccountQuery;

import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class AccountRepository {

    private final InsertAccountQuery insertAccountQuery;
    private final GetAccountQuery getAccountQuery;

    public void saveAccountInfo(List<NumericAssetBalance> assetBalances) {
        try {
            insertAccountQuery.execute(assetBalances);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public List<NumericAssetBalance> getAccountInfo(long time, Ticker ticker) {
        return getAccountQuery.getBalances(time, ticker);
    }
}
