package ru.mechmat.azzgzz.repository.sql;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.sqlite.JDBC;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;

import java.util.Date;
import java.util.List;

import static ru.mechmat.azzgzz.repository.sql.InsertCandlestickQuery.TICKERS_TABLE;
import static ru.mechmat.azzgzz.service.TradingProcessService.HOUR_TIME_STEP;

@Component
@RequiredArgsConstructor
public class SelectCandlestickQuery {

    private final JdbcTemplate jdbcTemplate;

    private static final String SELECT_FROM_TICKERS =
            "select * from incoming";

    private static final String SELECT_ONE_FROM_TIME =
            "select * from incoming " +
                    "where ? >= open_time -100 and ? < close_time";
    private static final String SELECT_HOUR_FROM_TIME =
            "select * from " + TICKERS_TABLE +
                    " where 1=1" +
                    " and ticker = ?" +
                    " and open_time >= ?" +
                    " and open_time < ? + " + HOUR_TIME_STEP + " " +
                    " order by open_time";// +
//                    " limit 60";

    @Deprecated
    public List<NumericCandlestick> execute() {
        return jdbcTemplate.query(SELECT_FROM_TICKERS, BeanPropertyRowMapper.newInstance(NumericCandlestick.class));
    }

    @Deprecated
    public NumericCandlestick executeOne(long openTime) {
        return jdbcTemplate.queryForObject(SELECT_ONE_FROM_TIME, new Object[] {openTime, openTime}, BeanPropertyRowMapper.newInstance(NumericCandlestick.class));
    }

    public List<NumericCandlestick> execute(Ticker ticker, long openTime) {
//        jdbcTemplate.queryForRowSet()
        return jdbcTemplate.query(SELECT_HOUR_FROM_TIME, new Object[] {ticker.ordinal(), openTime, openTime}, BeanPropertyRowMapper.newInstance(NumericCandlestick.class));
    }



}
