package ru.mechmat.azzgzz.repository.sql;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;

import java.util.List;

@Component
public class InsertAccountQuery {

    private final SimpleJdbcInsert simpleJdbcInsert;
    private static final String ACCOUNT_TABLE = "account";
    private static final String ASSET = "asset";
    private static final String FREE = "free";
    private static final String LOCKED = "locked";
    private static final String TIME = "time";

    public InsertAccountQuery(JdbcTemplate jdbcTemplate) {
        simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(ACCOUNT_TABLE)
                .usingColumns(ASSET, FREE, LOCKED, TIME);
    }

    public int[] execute(List<NumericAssetBalance> assetBalanceList) {
        return simpleJdbcInsert.executeBatch(
                assetBalanceList.stream()
                        .map(this::map)
                        .toArray(MapSqlParameterSource[]::new)
        );
    }

    private MapSqlParameterSource map(NumericAssetBalance numericAssetBalance) {
        return new MapSqlParameterSource()
                .addValue(ASSET, numericAssetBalance.getAsset())
                .addValue(FREE, numericAssetBalance.getFree())
                .addValue(LOCKED, numericAssetBalance.getLocked())
                .addValue(TIME, numericAssetBalance.getTime());
    }
}
