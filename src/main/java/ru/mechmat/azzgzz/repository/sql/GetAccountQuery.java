package ru.mechmat.azzgzz.repository.sql;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;

import java.util.List;

@Component
@RequiredArgsConstructor
public class GetAccountQuery {

    private final JdbcTemplate jdbcTemplate;

    private static final String SQL_SELECT =
            "select * from account " +
                    " where 1=1" +
                    " and time = ?" +
                    " and asset in (?, ?)";

    private static final String SQL_SELECT_ALL_WITH_TIME =
            "select * from account" +
                    " where time = ?";

    public List<NumericAssetBalance> getBalances(long time, Ticker ticker) {
        return jdbcTemplate.query(SQL_SELECT, new Object[]{time, ticker.getBaseAsset(), ticker.getQuoteAsset()},
                BeanPropertyRowMapper.newInstance(NumericAssetBalance.class));
    }

    public List<NumericAssetBalance> getBalances(long time) {
        return jdbcTemplate.query(SQL_SELECT_ALL_WITH_TIME, new Object[] {time}, BeanPropertyRowMapper.newInstance(NumericAssetBalance.class));
    }

    public List<NumericAssetBalance> getBalances() {
        return jdbcTemplate.query("select * from account", BeanPropertyRowMapper.newInstance(NumericAssetBalance.class));
    }
}
