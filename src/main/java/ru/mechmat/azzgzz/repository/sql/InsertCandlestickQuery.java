package ru.mechmat.azzgzz.repository.sql;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class InsertCandlestickQuery {

    public static final String TICKERS_TABLE = "incoming";
    private final SimpleJdbcInsert simpleJdbcInsert;

    private static final SimpleDateFormat visualDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public static final String OPEN_TIME = "open_time";
    public static final String OPEN = "open";
    public static final String HIGH = "high";
    public static final String LOW = "low";
    public static final String CLOSE = "close";
    public static final String VOLUME = "volume";
    public static final String CLOSE_TIME = "close_time";
    public static final String QUOTE_ASSET_VOLUME = "quote_asset_value";
    public static final String NUMBER_OF_TRADES = "number_of_trades";
    public static final String TAKER_BUY_BASE_ASSET_VOLUME = "taker_buy_base_asset_volume";
    public static final String TAKER_BUY_QUOTE_ASSET_VOLUME = "taker_buy_quote_asset_volume";
    public static final String TICKER = "ticker";
    public static final String VISUAL_DATE = "visual_date";

    public InsertCandlestickQuery(JdbcTemplate jdbcTemplate) {
        simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(TICKERS_TABLE)
                .usingColumns(OPEN_TIME, OPEN, HIGH, LOW, CLOSE, VOLUME, CLOSE_TIME,
                        QUOTE_ASSET_VOLUME, NUMBER_OF_TRADES, TAKER_BUY_BASE_ASSET_VOLUME,
                        TAKER_BUY_QUOTE_ASSET_VOLUME, TICKER, VISUAL_DATE);
    }

    public int execute(NumericCandlestick candlestick) {
        return simpleJdbcInsert.execute(mapToSqlParameterSource(candlestick));
    }

    private MapSqlParameterSource mapToSqlParameterSource(NumericCandlestick candlestick) {
        return new MapSqlParameterSource()
                .addValue(OPEN_TIME, candlestick.getOpenTime())
                .addValue(OPEN, candlestick.getOpen())
                .addValue(HIGH, candlestick.getHigh())
                .addValue(LOW, candlestick.getLow())
                .addValue(CLOSE, candlestick.getClose())
                .addValue(CLOSE_TIME, candlestick.getCloseTime())
                .addValue(VOLUME, candlestick.getVolume())
                .addValue(QUOTE_ASSET_VOLUME, candlestick.getQuoteAssetVolume())
                .addValue(NUMBER_OF_TRADES, candlestick.getNumberOfTrades())
                .addValue(TAKER_BUY_BASE_ASSET_VOLUME, candlestick.getTakerBuyBaseAssetVolume())
                .addValue(TAKER_BUY_QUOTE_ASSET_VOLUME, candlestick.getTakerBuyQuoteAssetVolume())
                .addValue(TICKER, candlestick.getTicker().ordinal())
                .addValue(VISUAL_DATE, visualDateFormat.format(new Date(candlestick.getOpenTime())));
    }

    public int[] execute(List<NumericCandlestick> candlestickList) {
        return simpleJdbcInsert.executeBatch(
                candlestickList.stream()
                        .map(this::mapToSqlParameterSource)
                        .toArray(MapSqlParameterSource[]::new)
        );
    }
}
