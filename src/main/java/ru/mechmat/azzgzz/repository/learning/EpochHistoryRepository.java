package ru.mechmat.azzgzz.repository.learning;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.mechmat.azzgzz.entity.ui.EpochResult;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
@Repository
public class EpochHistoryRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert jdbcInsert;

    public static final String TABLE_EPOCH_HISTORY = "epochs_history";
    public static final String AGENT_ID = "agent_id";
    public static final String START_VALUE = "start_value";
    public static final String END_VALUE = "end_value";
    public static final String EPOCH_ID = "epoch_id";
    public static final String DELTA = "delta";

    public static final String SQL_SELECT_BY_AGENT = "select * from epochs_history " +
            "where agent_id = ? " +
            "order by epoch_id";

    private static final String SQL_UPDATE_EPOCHS_RESULT = "update epochs_history " +
            "set start_value = ?, " +
            "end_value = ?, " +
            "delta = ?" +
            "where agent_id = ? " +
            "and epoch_id = ?";

    public EpochHistoryRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(TABLE_EPOCH_HISTORY)
                .usingColumns(AGENT_ID, START_VALUE, END_VALUE, EPOCH_ID, DELTA);
    }

    public int insertEpochResult(EpochResult epochResult) {
        return jdbcInsert.execute(map(epochResult));
    }

    public List<EpochResult> getEpochResults(String agentId) {
        return jdbcTemplate.query(SQL_SELECT_BY_AGENT, new Object[]{agentId}, BeanPropertyRowMapper.newInstance(EpochResult.class));
    }

    private MapSqlParameterSource map(EpochResult epochResult) {
        return new MapSqlParameterSource()
                .addValue(AGENT_ID, epochResult.getAgentId())
                .addValue(START_VALUE, epochResult.getStartValue())
                .addValue(END_VALUE, epochResult.getEndValue())
                .addValue(EPOCH_ID, epochResult.getEpochId())
                .addValue(DELTA, epochResult.getDelta());
    }

    public void updateEpochResult(EpochResult epochResult) {
        jdbcTemplate.update(SQL_UPDATE_EPOCHS_RESULT, epochResult.getStartValue(), epochResult.getEndValue(),
                epochResult.getEndValue()
                        .divide(epochResult.getStartValue(), 4, RoundingMode.HALF_UP)
                        .subtract(BigDecimal.ONE)
                        .multiply(BigDecimal.valueOf(100)),
                epochResult.getAgentId(), epochResult.getEpochId());
    }

    public void deleteAllAboutAgent(String agentId) {
        jdbcTemplate.execute("delete from epochs_history where agent_id = '" + agentId + "'");
    }
}
