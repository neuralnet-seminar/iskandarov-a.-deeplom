package ru.mechmat.azzgzz.repository.learning;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static ru.mechmat.azzgzz.service.TradingProcessService.HOUR_TIME_STEP;

@Slf4j
@Repository
public class AgentLearningAccountRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert jdbcInsert;

    private static final String ACCOUNT_TABLE = "agent_learning_account";
    private static final String TIME = "time";
    private static final String AGENT_ID = "agent_id";
    private static final String ASSET = "asset_qty";
    private static final String CASH = "cash_qty";

    private static final String SQL_SELECT = "select * from agent_learning_account " +
            "where time = ?";

    public AgentLearningAccountRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(ACCOUNT_TABLE)
                .usingColumns(TIME, AGENT_ID, ASSET, CASH);
    }

    public int insertAccount(List<NumericAssetBalance> assetBalanceList) {
        return jdbcInsert.execute(map(assetBalanceList));
    }

    private MapSqlParameterSource map(List<NumericAssetBalance> numericAssetBalance) {
        return new MapSqlParameterSource()
                .addValue(TIME, numericAssetBalance.get(0).getTime())
                .addValue(AGENT_ID, numericAssetBalance.get(0).getAgentId())
                .addValue(ASSET, numericAssetBalance.stream()
                        .filter(x -> Ticker.ETHUSDT.getBaseAsset().equals(x.getAsset()))
                        .findAny().get().getFree())
                .addValue(CASH, numericAssetBalance.stream()
                        .filter(x -> Ticker.ETHUSDT.getQuoteAsset().equals(x.getAsset()))
                        .findAny().get().getFree());
    }

    public List<NumericAssetBalance> getAccount(long time) {
        return convert(jdbcTemplate
                .query(SQL_SELECT, new Object[]{time}, BeanPropertyRowMapper.newInstance(AccountRow.class))
                .get(0));
    }

    private List<NumericAssetBalance> convert(AccountRow accountRow) {
        return Arrays.asList(
                new NumericAssetBalance()
                        .setTime(accountRow.getTime())
                        .setAsset(Ticker.ETHUSDT.getBaseAsset())
                        .setFree(accountRow.getAssetQty())
                        .setLocked(BigDecimal.ZERO)
                        .setAgentId(accountRow.getAgentId()),
                new NumericAssetBalance()
                        .setTime(accountRow.getTime())
                        .setAsset(Ticker.ETHUSDT.getQuoteAsset())
                        .setFree(accountRow.getCashQty())
                        .setLocked(BigDecimal.ZERO)
                        .setAgentId(accountRow.getAgentId())
        );
    }

    public void duplicateLastRowWithNextHour() {
        AccountRow accountRow = jdbcTemplate.query("select * from agent_learning_account " +
                "where time = (select max(time) from agent_learning_account)", BeanPropertyRowMapper.newInstance(AccountRow.class))
                .get(0);
        jdbcInsert.execute(
                new MapSqlParameterSource()
                        .addValue(TIME, accountRow.getTime() + HOUR_TIME_STEP)
                        .addValue(AGENT_ID, accountRow.getAgentId())
                        .addValue(ASSET, accountRow.getAssetQty())
                        .addValue(CASH, accountRow.getCashQty())
        );
    }

    public void deleteAccountData() {
        jdbcTemplate.execute("delete from agent_learning_account" +
                " where 1=1");
    }

    public Long getCurrentTime() {
        return jdbcTemplate.queryForObject("select max(time) from agent_learning_account", Long.class);
    }

    @Data
    @Accessors(chain = true)
    static class AccountRow {

        private long time;
        private String agentId;
        private BigDecimal assetQty;
        private BigDecimal cashQty;
    }
}
