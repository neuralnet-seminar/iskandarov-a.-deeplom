package ru.mechmat.azzgzz.repository.learning;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.mechmat.azzgzz.entity.learning.TestRow;

import java.util.List;

@Repository
public class TestGraphicRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert jdbcInsert;

    public static final String TIME = "time";
    public static final String AGENT_ID = "agent_id";
    public static final String VALUE = "value";

    public static final String SQL_SELECT_BY_AGENT_BY_DAY = "select * from tests_graphic " +
            "where agent_id = ? " +
            "and mod(time + 1000*60*60*3, 1000*60*60*24) = 0 " +
            "order by time";

    public TestGraphicRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("tests_graphic")
                .usingColumns(TIME, AGENT_ID, VALUE);
    }

    public int insertRow(TestRow row) {
        return jdbcInsert.execute(map(row));
    }

    public int[] insertRows(List<TestRow> rows) {
        return jdbcInsert.executeBatch(rows.stream()
                .map(this::map)
                .toArray(MapSqlParameterSource[]::new)
        );
    }

    private MapSqlParameterSource map(TestRow row) {
        return new MapSqlParameterSource()
                .addValue(TIME, row.getTime())
                .addValue(AGENT_ID, row.getAgentId())
                .addValue(VALUE, row.getValue());
    }

    public List<TestRow> getAllTestRows(String agentId) {
        return jdbcTemplate.query(SQL_SELECT_BY_AGENT_BY_DAY, new Object[]{agentId}, BeanPropertyRowMapper.newInstance(TestRow.class));
    }

    public List<String> getTestedAgents() {
        return jdbcTemplate.queryForList("select distinct agent_id from tests_graphic order by agent_id", String.class);
    }

    public void cloneDataFromCurrentEpoch() {
        jdbcTemplate.execute("insert into tests_graphic " +
                "select ce.time, ce.agent_id, agent_value from current_epoch ce");
    }

    public void clearAgentData(String agentId) {
        jdbcTemplate.execute("delete from tests_graphic where agent_id = '" + agentId + "'");
    }
}
