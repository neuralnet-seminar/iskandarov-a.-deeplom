package ru.mechmat.azzgzz.repository.learning;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.mechmat.azzgzz.entity.learning.CurrentEpochRow;
import ru.mechmat.azzgzz.entity.ui.EpochCurrentRows;

import java.math.BigDecimal;

@Slf4j
@Repository
public class CurrentEpochRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert jdbcInsert;

    private static final String CURRENT_EPOCH_TABLE = "current_epoch";

    private static final String TIME = "time";
    private static final String AGENT_ID = "agent_id";
    private static final String ASSET_RATIO = "asset_ratio";
    private static final String ASSET_VALUE = "asset_value";
    private static final String AGENT_VALUE = "agent_value";

    private static final String SQL_SELECT_ALL_BY_DAYS = "select time, asset_ratio, asset_value, agent_value " +
            "from current_epoch " +
            "where mod(time + 1000*60*60*5, 1000*60*60*24) = 0 " +
            "order by time";

    private static final String SELECT_AGENT_VALUE = "select agent_value from current_epoch " +
            "where time = (select %s(e.time) from current_epoch e)";

    public CurrentEpochRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(CURRENT_EPOCH_TABLE)
                .usingColumns(TIME, AGENT_ID, ASSET_RATIO, ASSET_VALUE, AGENT_VALUE);
    }

    public Boolean deleteEpochData() {
        try {
            jdbcTemplate.execute("delete from current_epoch where 1=1");
            return true;
        } catch (DataAccessException e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public int insertCurrentEpochRow(String agentId, EpochCurrentRows.Row currentRows) {
        return jdbcInsert.execute(mapToSqlParameterSource(agentId, currentRows));
    }

    public int[] insertCurrentEpochRows(EpochCurrentRows currentRows) {
        String agentId = currentRows.getAgentId();
        return jdbcInsert.executeBatch(currentRows.getValues().stream()
                .map(x -> mapToSqlParameterSource(agentId, x))
                .toArray(MapSqlParameterSource[]::new)
        );
    }

    private MapSqlParameterSource mapToSqlParameterSource(String agentId, EpochCurrentRows.Row currentRow) {
        return new MapSqlParameterSource()
                .addValue(TIME, currentRow.getTime())
                .addValue(AGENT_ID, agentId)
                .addValue(ASSET_RATIO, currentRow.getAssetRatio())
                .addValue(ASSET_VALUE, currentRow.getAssetValue())
                .addValue(AGENT_VALUE, currentRow.getAgentValue());
    }

    public EpochCurrentRows getAllRowsOfCurrentEpoch() {
        String agentId = jdbcTemplate.queryForObject("select agent_id from current_epoch limit 1", String.class);
        return new EpochCurrentRows()
                .setAgentId(agentId)
                .setValues(jdbcTemplate.query(SQL_SELECT_ALL_BY_DAYS, BeanPropertyRowMapper.newInstance(EpochCurrentRows.Row.class)));
    }

    public BigDecimal getStartAgentValue() {
        return jdbcTemplate.queryForObject(String.format(SELECT_AGENT_VALUE, "min"), BigDecimal.class);
    }

    public BigDecimal getEndAgentValue() {
        return jdbcTemplate.queryForObject(String.format(SELECT_AGENT_VALUE, "max"), BigDecimal.class);
    }

    public Long getCurrentTime() {
        return jdbcTemplate.queryForObject("select max(time) from current_epoch", Long.class);
    }

    public BigDecimal getAgentValue(String agentId, long time) {
        return jdbcTemplate.queryForObject("select agent_value from current_epoch" +
                " where time = ?", new Object[]{time}, BigDecimal.class);
    }

    public CurrentEpochRow getRow(long time) {
        return jdbcTemplate.query("select * from current_epoch where time = ?", new Object[]{time},
                BeanPropertyRowMapper.newInstance(CurrentEpochRow.class))
                .get(0);
    }
}
