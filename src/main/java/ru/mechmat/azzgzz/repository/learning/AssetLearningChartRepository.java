package ru.mechmat.azzgzz.repository.learning;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.mechmat.azzgzz.entity.learning.TestRow;
import ru.mechmat.azzgzz.entity.ui.EpochChart;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class AssetLearningChartRepository {

    private final JdbcTemplate jdbcTemplate;

    private static final String SELECT_MIN_LEARN_TIME =
            "select min(time) from asset_learning_chart " +
            "where status = 'LEARN'";

    private static final String SELECT_MAX_LEARN_TIME =
            "select max(time) from asset_learning_chart " +
                    "where status = 'LEARN'";

    private static final String SELECT_MIN_TEST_TIME =
            "select min(time) from asset_learning_chart " +
                    "where status = 'TEST'";

    private static final String SELECT_MAX_TEST_TIME =
            "select max(time) from asset_learning_chart " +
                    "where status = 'TEST'";

    public static final String SQL_SELECT_ALL_TEST_BY_DAY = "select time, value from asset_learning_chart " +
            "where status = 'TEST' " +
            "and mod(time + 3*60*60*1000, 1000*60*60*24) = 0 " +
            "order by time";


    @Cacheable(cacheNames = SELECT_MIN_LEARN_TIME, key = "'" + SELECT_MIN_LEARN_TIME + "'")
    public Long getFirstLearningTime() {
        return jdbcTemplate.queryForObject(SELECT_MIN_LEARN_TIME, Long.class);
    }


    @Cacheable(cacheNames = SELECT_MAX_LEARN_TIME, key = "'" + SELECT_MAX_LEARN_TIME + "'")
    public Long getLastLearningTime() {
        return jdbcTemplate.queryForObject(SELECT_MAX_LEARN_TIME, Long.class);
    }


    @Cacheable(cacheNames = SELECT_MIN_TEST_TIME, key = "'" + SELECT_MIN_TEST_TIME + "'")
    public Long getFirstTestTime() {
        return jdbcTemplate.queryForObject(SELECT_MIN_TEST_TIME, Long.class);
    }


    @Cacheable(cacheNames = SELECT_MAX_TEST_TIME, key = "'" + SELECT_MAX_TEST_TIME + "'")
    public Long getLastTestTime() {
        return jdbcTemplate.queryForObject(SELECT_MAX_TEST_TIME, Long.class);
    }

    public EpochChart.Row getLearnRow(Long time) {
        return jdbcTemplate.query("select time, value from asset_learning_chart " +
                "where status = 'LEARN' and time = ?", new Object[] {time}, BeanPropertyRowMapper.newInstance(EpochChart.Row.class))
                .get(0);
    }

    public EpochChart.Row getTestRow(Long time) {
        return jdbcTemplate.query("select time, value from asset_learning_chart " +
                "where status = 'TEST' and time = ?", new Object[] {time}, BeanPropertyRowMapper.newInstance(EpochChart.Row.class))
                .get(0);
    }

    @Cacheable(cacheNames = "assetLearnDelta", key = "'assetLearnDelta'")
    public BigDecimal getAssetLearnDelta() {
        BigDecimal startLearnValue = jdbcTemplate.queryForObject(
                "select value from asset_learning_chart " +
                        "where status = 'LEARN' " +
                        "and time = (" + SELECT_MIN_LEARN_TIME + ")",
                BigDecimal.class);
        BigDecimal endLearnValue = jdbcTemplate.queryForObject(
                "select value from asset_learning_chart " +
                        "where status = 'LEARN' " +
                        "and time = (" + SELECT_MAX_LEARN_TIME + ")",
                BigDecimal.class);
        return endLearnValue.divide(startLearnValue, 2, RoundingMode.HALF_UP)
                .subtract(BigDecimal.ONE)
                .multiply(BigDecimal.valueOf(100))
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Cacheable(cacheNames = "allAssetTestChart", key = "'allAssetTestChart'")
    public List<TestRow> getAllAssetTestRows() {
        return jdbcTemplate.query(SQL_SELECT_ALL_TEST_BY_DAY, BeanPropertyRowMapper.newInstance(TestRow.class));
    }

}
