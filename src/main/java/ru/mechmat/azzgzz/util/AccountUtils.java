package ru.mechmat.azzgzz.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericAccount;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;

import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountUtils {

    public static BigDecimal getVolume(NumericAccount account, BigDecimal price, Ticker ticker) {
        BigDecimal coinQty = getAssetQty(account, ticker.getBaseAsset());
        BigDecimal cash = getAssetQty(account, ticker.getQuoteAsset());
        return cash.add(coinQty.multiply(price));
    }

    public static BigDecimal getAssetQty(NumericAccount account, String quoteAsset) {
        return account.getBalances()
                .stream()
                .filter(x -> quoteAsset.equals(x.getAsset()))
                .map(NumericAssetBalance::getFree)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal getAssetQty(List<NumericAssetBalance> balances, String quoteAsset) {
        return balances
                .stream()
                .filter(x -> quoteAsset.equals(x.getAsset()))
                .map(NumericAssetBalance::getFree)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
