package ru.mechmat.azzgzz.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.temporal.ChronoUnit;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TimeUtils {

    public static Long truncate(Long millis, ChronoUnit chronoUnit) {
        assert ChronoUnit.SECONDS.equals(chronoUnit) || ChronoUnit.MINUTES.equals(chronoUnit);
        long multiplier = chronoUnit.getDuration().getSeconds() * 1000;
        return millis / multiplier * multiplier;
    }

    public static Long truncate(Long millis) {
        return truncate(millis, ChronoUnit.SECONDS);
    }
}
