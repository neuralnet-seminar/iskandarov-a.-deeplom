package ru.mechmat.azzgzz.web.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.mechmat.azzgzz.entity.ui.*;
import ru.mechmat.azzgzz.service.ui.UiEpochService;
import ru.mechmat.azzgzz.service.ui.UiEpochsComparingService;
import ru.mechmat.azzgzz.service.ui.UiTestChartService;

@Slf4j
@RestController
@RequestMapping("/api/ui")
@RequiredArgsConstructor
public class UiController {

    private final UiEpochService uiEpochService;
    private final UiEpochsComparingService uiEpochsComparingService;
    private final UiTestChartService uiTestChartService;

    @GetMapping("/create_agent/{id}")
    public Boolean createAgent(@PathVariable String id) {
        return uiEpochService.createAgent(id);
    }

    @GetMapping("/get_current_epoch_state")
    public EpochCurrentRows getCurrentEpochState() {
        return uiEpochService.getCurrentEpochState();
    }

    @GetMapping("/get_next_rows")
    public EpochCurrentRows getRow(@RequestParam Integer hours) throws InterruptedException {
        return uiEpochService.getNextEpochsState(hours);
    }

    @GetMapping("/is_epoch_ended")
    public Boolean isEpochEnded() {
        return uiEpochService.getEpochEnded();
    }

    @GetMapping("/go_next_epoch")
    public ResponseContainer<Boolean> goNextEpoch() {
        return ResponseContainer.of(uiEpochService.goNextEpoch());
    }

    @GetMapping("/get_epochs_comparing/{id}")
    public EpochComparingResults getEpochsComparing(@PathVariable String id) {
        return new EpochComparingResults()
                .setValues(uiEpochsComparingService.getEpochComparingChart(id));
    }

    @GetMapping("/go_full_epoch")
    public Boolean goFullEpoch() {
        uiEpochService.goFullEpoch();
        return true;
    }

    @GetMapping("/get_learn_progress")
    public Long getLearnProgress() {
        return uiEpochService.getLearnProgress();
    }

    @GetMapping("/get_test_chart")
    public TestChart getTestChart() {
        return uiTestChartService.getTestChart();
    }

    @GetMapping("/go_test/{id}")
    public Boolean goTest(@PathVariable String id) {
        uiTestChartService.goTest(id);
        return true;
    }

    @GetMapping("/get_test_progress")
    public Integer getTestProgress() {
        return uiTestChartService.getTestProgress();
    }

    @GetMapping("/reset_test_progress")
    public Boolean resetTestProgress() {
        uiTestChartService.setTestProgress(0);
        return true;
    }
}
