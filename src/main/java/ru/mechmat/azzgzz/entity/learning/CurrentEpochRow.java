package ru.mechmat.azzgzz.entity.learning;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class CurrentEpochRow {

    private long time;

    private String agentId;

    private BigDecimal assetRatio;

    private BigDecimal assetValue;

    private BigDecimal agentValue;
}
