package ru.mechmat.azzgzz.entity.learning;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class TestRow {

    private Long time;
    private String agentId;
    private BigDecimal value;
}
