package ru.mechmat.azzgzz.entity.market;

import com.binance.api.client.domain.account.Trade;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.mechmat.azzgzz.converter.OrderConverter;
import ru.mechmat.azzgzz.dictionary.Ticker;

import java.math.BigDecimal;

import static ru.mechmat.azzgzz.converter.OrderConverter.getNullable;

@Data
@Accessors(chain = true)
public class NumericTrade {


    /**
     * Trade id.
     */
    private Long id;

    /**
     * Price.
     */
    private BigDecimal price;

    /**
     * Quantity.
     */
    private BigDecimal qty;


    /**
     * Quote quantity for the trade (price * qty).
     */
    private BigDecimal quoteQty;

    /**
     * Commission.
     */
    private BigDecimal commission;

    /**
     * Asset on which commission is taken
     */
    private String commissionAsset;

    /**
     * Trade execution time.
     */
    private long time;

    /**
     * The symbol of the trade.
     */
    private Ticker symbol;

    @JsonProperty("isBuyer")
    private boolean buyer;

    @JsonProperty("isMaker")
    private boolean maker;

    @JsonProperty("isBestMatch")
    private boolean bestMatch;

    private String orderId;

    public NumericTrade(Trade trade) {
        id = trade.getId();
        price = getNullable(trade.getPrice());
        qty = getNullable(trade.getQty());
        quoteQty = getNullable(trade.getQuoteQty());
        commission = getNullable(trade.getCommission());
        commissionAsset = trade.getCommissionAsset();
        time = trade.getTime();
        symbol = Ticker.fromValue(trade.getSymbol());
        buyer = trade.isBuyer();
        maker = trade.isMaker();
        bestMatch = trade.isBestMatch();
        orderId = trade.getOrderId();
    }
}
