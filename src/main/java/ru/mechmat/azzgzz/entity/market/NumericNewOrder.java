package ru.mechmat.azzgzz.entity.market;

import com.binance.api.client.domain.OrderSide;
import com.binance.api.client.domain.OrderType;
import com.binance.api.client.domain.TimeInForce;
import com.binance.api.client.domain.account.NewOrder;
import com.binance.api.client.domain.account.NewOrderResponseType;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.mechmat.azzgzz.dictionary.Ticker;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
@Accessors(chain = true)
public class NumericNewOrder {

    private Ticker ticker;

    private OrderSide side;

    private OrderType type;

    private TimeInForce timeInForce;

    private BigDecimal quantity;

    private BigDecimal price;

    private String newClientOrderId;

    private BigDecimal stopPrice;

    private BigDecimal icebergQty;

    private NewOrderResponseType newOrderRespType;

    private Long recvWindow;

    private long timestamp;

    public static NumericNewOrder marketBuy(Ticker ticker, BigDecimal quantity) {
        return new NumericNewOrder()
                .setTicker(ticker)
                .setQuantity(quantity.setScale(8, RoundingMode.HALF_UP))
                .setType(OrderType.MARKET)
                .setSide(OrderSide.BUY);
    }

    public static NumericNewOrder marketSell(Ticker ticker, BigDecimal quantity) {
        return new NumericNewOrder()
                .setTicker(ticker)
                .setQuantity(quantity.setScale(8, RoundingMode.HALF_UP))
                .setSide(OrderSide.SELL)
                .setType(OrderType.MARKET);
    }
}
