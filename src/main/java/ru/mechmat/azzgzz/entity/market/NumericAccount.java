package ru.mechmat.azzgzz.entity.market;

import com.binance.api.client.domain.account.Account;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Accessors(chain = true)
public class NumericAccount {

    private int makerCommission;
    private int takerCommission;
    private int buyerCommission;
    private int sellerCommission;
    private boolean canTrade;
    private boolean canWithdraw;
    private boolean canDeposit;
    private long updateTime;
    private List<NumericAssetBalance> balances;

    public NumericAccount(Account account) {
        makerCommission = account.getMakerCommission();
        takerCommission = account.getTakerCommission();
        buyerCommission = account.getBuyerCommission();
        sellerCommission = account.getSellerCommission();
        canTrade = account.isCanTrade();
        canWithdraw = account.isCanWithdraw();
        canDeposit = account.isCanDeposit();
        updateTime = account.getUpdateTime();
        balances = Optional.ofNullable(account.getBalances())
                .map(Collection::stream)
                .orElse(Stream.empty())
                .map(NumericAssetBalance::new)
                .collect(Collectors.toList());;
    }
}
