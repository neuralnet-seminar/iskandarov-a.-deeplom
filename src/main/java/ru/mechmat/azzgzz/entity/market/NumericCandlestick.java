package ru.mechmat.azzgzz.entity.market;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.mechmat.azzgzz.dictionary.Ticker;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class NumericCandlestick {

    @NotNull
    private Ticker ticker;

    private long openTime;

    private BigDecimal open;

    private BigDecimal high;

    private BigDecimal low;

    private BigDecimal close;

    private BigDecimal volume;

    private long closeTime;

    private BigDecimal quoteAssetVolume;

    private BigDecimal numberOfTrades;

    private String takerBuyBaseAssetVolume;

    private String takerBuyQuoteAssetVolume;
}
