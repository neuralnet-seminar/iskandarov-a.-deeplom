package ru.mechmat.azzgzz.entity.market;

import com.binance.api.client.domain.account.AssetBalance;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class NumericAssetBalance {

    private String asset;

    private BigDecimal free;

    private BigDecimal locked;

    private Long time;

    private String agentId;

    public NumericAssetBalance(AssetBalance balance) {
        asset = balance.getAsset();
        free = new BigDecimal(balance.getFree());
        locked = new BigDecimal(balance.getLocked());
    }
}
