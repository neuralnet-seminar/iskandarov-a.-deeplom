package ru.mechmat.azzgzz.entity.market;

import com.binance.api.client.domain.OrderSide;
import com.binance.api.client.domain.OrderStatus;
import com.binance.api.client.domain.OrderType;
import com.binance.api.client.domain.TimeInForce;
import com.binance.api.client.domain.account.NewOrderResponse;
import com.binance.api.client.domain.account.Trade;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.mechmat.azzgzz.dictionary.Ticker;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ru.mechmat.azzgzz.converter.OrderConverter.getNullable;

@Data
@Accessors(chain = true)
public class NumericNewOrderResponse {


    /**
     * Order symbol.
     */
    private Ticker symbol;

    /**
     * Order id.
     */
    private Long orderId;

    /**
     * This will be either a generated one, or the newClientOrderId parameter
     * which was passed when creating the new order.
     */
    private String clientOrderId;

    private BigDecimal price;

    private BigDecimal origQty;

    private BigDecimal executedQty;

    private BigDecimal cummulativeQuoteQty;

    private OrderStatus status;

    private TimeInForce timeInForce;

    private OrderType type;

    private OrderSide side;

    // @JsonSetter(nulls = Nulls.AS_EMPTY)
    private List<NumericTrade> fills;

    /**
     * Transact time for this order.
     */
    private Long transactTime;

    public NumericNewOrderResponse(NewOrderResponse response) {
        symbol = Ticker.fromValue(response.getSymbol());
        orderId = response.getOrderId();
        clientOrderId = response.getClientOrderId();
        price = getNullable(response.getPrice());
        origQty = getNullable(response.getOrigQty());
        executedQty = getNullable(response.getExecutedQty());
        cummulativeQuoteQty = getNullable(response.getCummulativeQuoteQty());
        status = response.getStatus();
        timeInForce = response.getTimeInForce();
        type = response.getType();
        side = response.getSide();
        fills = Optional.ofNullable(response.getFills())
                .map(Collection::stream)
                .orElse(Stream.empty())
                .map(NumericTrade::new)
                .collect(Collectors.toList());
        transactTime = response.getTransactTime();
    }
}
