package ru.mechmat.azzgzz.entity.ui;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;
import lombok.Value;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Value(staticConstructor = "of")
@Accessors(chain = true)
public class ResponseContainer<T> {

    LocalDateTime time = LocalDateTime.now();

    T data;

}
