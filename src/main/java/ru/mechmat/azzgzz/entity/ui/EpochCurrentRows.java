package ru.mechmat.azzgzz.entity.ui;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Класс для отображения линейного графика
 */
@Data
@Accessors(chain = true)
public class EpochCurrentRows {

    private List<Row> values;

    private String agentId;

    @Data
    @Accessors(chain = true)
    public static class Row {

        static SimpleDateFormat rowDateFormat = new SimpleDateFormat("dd.MM.yyyy-HH");

        long time;

        BigDecimal assetRatio;

        BigDecimal assetValue;

        BigDecimal agentValue;

        @JsonValue
        public List getRow() {

            return Arrays.asList(
                    rowDateFormat.format(new Date(time)),
                    assetRatio.setScale(2, RoundingMode.HALF_UP),
                    assetValue.setScale(2, RoundingMode.HALF_UP),
                    agentValue.setScale(2, RoundingMode.HALF_UP)
            );
        }
    }
}
