package ru.mechmat.azzgzz.entity.ui;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class TestChart {

    List<Row> values;

    @Data
    @Accessors(chain = true)
    public static class Row {

        @JsonValue
        List row;
    }
}
