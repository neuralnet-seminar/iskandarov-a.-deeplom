package ru.mechmat.azzgzz.entity.ui;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Класс для отображения тестирования
 */
@Data
@Accessors(chain = true)
public class EpochChart {

    List<Row> values;
    private String agentId;
    private List<String> dates;

    @Data
    @Accessors(chain = true)
    public static class Row {

        private Long time;
        private BigDecimal value;

        @JsonValue
        public BigDecimal getRow() {
            return value.setScale(2, RoundingMode.HALF_UP);
        }
    }
}
