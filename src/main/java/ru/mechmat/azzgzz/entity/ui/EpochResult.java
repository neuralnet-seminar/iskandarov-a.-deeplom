package ru.mechmat.azzgzz.entity.ui;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Класс, для сравнения эпох обучения
 */
@Data
@Accessors(chain = true)
public class EpochResult {

    private String agentId;
    private BigDecimal startValue;
    private BigDecimal endValue;
    private Integer epochId;
    private BigDecimal delta;

    @JsonValue
    public List getValue() {
        return Arrays.asList(
                Optional.ofNullable(epochId).map(String::valueOf).map(x -> "Ep. " + x).orElse("Eth"),
                delta,
                String.valueOf(delta.setScale(0, RoundingMode.HALF_UP))
        );
    }
}
