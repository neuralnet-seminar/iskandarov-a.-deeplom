package ru.mechmat.azzgzz.entity.ui;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class EpochComparingResults {

    List<EpochResult> values;
}
