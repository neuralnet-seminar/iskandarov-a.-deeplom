package ru.mechmat.azzgzz.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.repository.IncomingRepository;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty("database.need-fill")
public class FillDatabaseService {

    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("dd/MM/yy HH:mm");
    private final IncomingRepository incomingRepository;

    private Reader reader;
    private CSVParser csvParser;

    private static final int BATCH_SIZE = 5000;

    private final List<NumericCandlestick> candlesticks = new ArrayList<>(BATCH_SIZE);

    public enum Headers {
        DATE, TIME, OPEN, HIGH, LOW, CLOSE, VOL;

        @Override
        public String toString() {
            return "<" + this.name() + ">";
        }
    }

    private void openCsv(String path) throws IOException {
        reader = Files.newBufferedReader(Paths.get(path));
        csvParser = CSVParser.parse(reader, CSVFormat.DEFAULT
                .withFirstRecordAsHeader());
    }

    private void closeCsv() throws IOException {
        csvParser.close();
        reader.close();
    }


    @Scheduled(fixedRate = 60 * 1000 * 60 * 74 + 10)
    public void readEthereumBase() throws IOException, ParseException {
        openCsv("src/python/main/Second/market_data/eth-usd-1min.csv");

        runAlgorithm();

        lastSave();
        closeCsv();
    }


    private Date parseDateTime(CSVRecord record) throws ParseException {
        return TIME_FORMAT.parse(record.get(Headers.DATE) + " " + record.get(Headers.TIME));
    }

    public void runAlgorithm() throws ParseException {
        Iterator<CSVRecord> iterator = csvParser.iterator();
        CSVRecord previousRecord = iterator.next();

        save(mapTo(previousRecord));
//        int i = 0;
        while (iterator.hasNext()) {
            CSVRecord currentRecord = iterator.next();
            save(generate(previousRecord, currentRecord));
            previousRecord = currentRecord;
        }
    }

    private NumericCandlestick mapTo(CSVRecord record) throws ParseException {
        long time = parseDateTime(record).getTime();
        return new NumericCandlestick()
                .setTicker(Ticker.ETHUSDT)
                .setOpenTime(time)
                .setOpen(new BigDecimal(record.get(Headers.OPEN)))
                .setHigh(new BigDecimal(record.get(Headers.HIGH)))
                .setLow(new BigDecimal(record.get(Headers.LOW)))
                .setClose(new BigDecimal(record.get(Headers.CLOSE)))
                .setCloseTime(time + 60_000)
                .setVolume(new BigDecimal(record.get(Headers.VOL)));
    }

    private List<NumericCandlestick> generate(CSVRecord fromExclusive, CSVRecord toInclusive) throws ParseException {
        long prevDate = parseDateTime(fromExclusive).getTime();
        long curDate = parseDateTime(toInclusive).getTime();
        List<NumericCandlestick> generatedList = IntStream.range(1, getGeneratedCount(prevDate, curDate))
                .mapToObj(i -> {
                            BigDecimal constantPrice = new BigDecimal(fromExclusive.get(Headers.CLOSE));
                            return new NumericCandlestick()
                                    .setTicker(Ticker.ETHUSDT)
                                    .setOpenTime(prevDate + i * 60_000)
                                    .setOpen(constantPrice)
                                    .setHigh(constantPrice)
                                    .setLow(constantPrice)
                                    .setClose(constantPrice)
                                    .setCloseTime(prevDate + (i + 1) * 60_000)
                                    .setVolume(BigDecimal.ZERO);
                        }
                ).collect(Collectors.toList());
        generatedList.add(mapTo(toInclusive));
        return generatedList;
    }

    private int getGeneratedCount(long a, long b) {
        return ((int) (b - a) / 1000) / 60;
    }

    private void save(NumericCandlestick candlestick) {
        checkCacheSize();
        candlesticks.add(candlestick);
    }

    private void save(List<NumericCandlestick> candlesticks) {
        checkCacheSize();
        this.candlesticks.addAll(candlesticks);
    }

    private void checkCacheSize() {
        if (candlesticks.size() > BATCH_SIZE) {
            incomingRepository.insertCandlestick(candlesticks);
            candlesticks.clear();
        }
    }

    private void lastSave(){
        incomingRepository.insertCandlestick(candlesticks);
    }
}
