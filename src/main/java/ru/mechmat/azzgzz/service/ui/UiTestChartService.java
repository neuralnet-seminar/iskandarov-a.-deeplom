package ru.mechmat.azzgzz.service.ui;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.mechmat.azzgzz.entity.learning.TestRow;
import ru.mechmat.azzgzz.entity.ui.TestChart;
import ru.mechmat.azzgzz.repository.learning.AssetLearningChartRepository;
import ru.mechmat.azzgzz.repository.learning.TestGraphicRepository;
import ru.mechmat.azzgzz.service.MarketLearningListener;
import ru.mechmat.azzgzz.service.learning.AgentService;
import ru.mechmat.azzgzz.service.learning.CurrentEpochService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UiTestChartService {

    private final TestGraphicRepository testGraphicRepository;
    private final AssetLearningChartRepository assetLearningChartRepository;
    private final AgentService agentService;
    private final CurrentEpochService currentEpochService;
    private final MarketLearningListener marketLearningListener;

    @Setter
    @Getter
    private int testProgress = 0;

    private static final Comparator<TestRow> TEST_ROW_COMPARATOR = Comparator.comparing(TestRow::getAgentId);
    private static final SimpleDateFormat TEST_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    public TestChart getTestChart() {
        List<TestRow> allAssetTestRows = assetLearningChartRepository.getAllAssetTestRows();
        BigDecimal coeff = allAssetTestRows.get(0).getValue().divide(BigDecimal.valueOf(100), 10, RoundingMode.HALF_UP);
        allAssetTestRows.forEach(x -> x.setValue(x.getValue().divide(coeff, 10, RoundingMode.HALF_UP)));

        List<String> testedAgents = testGraphicRepository.getTestedAgents();

        Map<Long, List<TestRow>> agentsChart = testedAgents.stream()
                .map(testGraphicRepository::getAllTestRows)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(
                        TestRow::getTime
                ));

        return new TestChart()
                .setValues(convertToTestChartRows(allAssetTestRows, agentsChart));
    }

    @Async
    public void goTest(String agentId) {
        agentService.prepareForTest(agentId);
        testGraphicRepository.clearAgentData(agentId);
        int availableTestSetSize = getAvailableTestSetSize();

        marketLearningListener.setSteps(0);
        for (int i = 0; i < availableTestSetSize; i++) {
            marketLearningListener.nextHour(agentId);
            testProgress = (i+1)*100/availableTestSetSize;
        }
        testGraphicRepository.cloneDataFromCurrentEpoch();
    }

    private int getAvailableTestSetSize() {
        return (int) ((assetLearningChartRepository.getLastTestTime() -
                currentEpochService.getCurrentEpochTime())/1000/60/60);
    }



    private List<TestChart.Row> convertToTestChartRows(List<TestRow> allAssetTestRows, Map<Long, List<TestRow>> agentsChart) {
        List<TestChart.Row> rows = new ArrayList<>(allAssetTestRows.size() + 2);
        TestChart.Row row = new TestChart.Row();
        List columns = new ArrayList(allAssetTestRows.size() + 2);
        columns.addAll(Arrays.asList("Дата", "Ethereum"));
        boolean anyAgentsTested = !CollectionUtils.isEmpty(agentsChart.values());
        if (anyAgentsTested) {
            columns.addAll(agentsChart.get(allAssetTestRows.get(0).getTime())
                    .stream()
                    .sorted(TEST_ROW_COMPARATOR)
                    .map(TestRow::getAgentId)
                    .collect(Collectors.toList())
            );
        }
        rows.add(row.setRow(columns));

        for (TestRow testRow : allAssetTestRows) {
            List rowValues = new ArrayList(8);
            rowValues.addAll(Arrays.asList(TEST_DATE_FORMAT.format(new Date(testRow.getTime())), testRow.getValue()));
            if (anyAgentsTested) {
                rowValues.addAll(agentsChart.get(testRow.getTime()).stream()
                        .sorted(TEST_ROW_COMPARATOR)
                        .map(TestRow::getValue)
                        .collect(Collectors.toList())
                );
            }
            rows.add(new TestChart.Row().setRow(rowValues));
        }
        return rows;
    }

}
