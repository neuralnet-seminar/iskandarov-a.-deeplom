package ru.mechmat.azzgzz.service.ui;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.entity.ui.EpochCurrentRows;
import ru.mechmat.azzgzz.repository.learning.AssetLearningChartRepository;
import ru.mechmat.azzgzz.service.MarketLearningListener;
import ru.mechmat.azzgzz.service.learning.AgentService;
import ru.mechmat.azzgzz.service.learning.CurrentEpochService;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Math.max;

@Slf4j
@Service
@RequiredArgsConstructor
public class UiEpochService {

    private final AssetLearningChartRepository assetLearningChartRepository;
    private final CurrentEpochService currentEpochService;
    private final AgentService agentService;
    private final MarketLearningListener marketLearningListener;


    @Getter
    private Boolean epochEnded = false;
    private String currentAgentId;

    @Setter
    private int learnProgress = 0;

    private long fullSize;

    @PostConstruct
    void prepare() {
        fullSize = (assetLearningChartRepository.getLastLearningTime() -
                assetLearningChartRepository.getFirstLearningTime()) / 1000 / 60 / 60;
    }

    public Boolean createAgent(String id) {
        currentAgentId = id;
        epochEnded = false;
        marketLearningListener.setSteps(0);
        return agentService.createAgent(id);
    }

    public EpochCurrentRows getCurrentEpochState() {
        return currentEpochService.getCurrentEpochAllRows();
    }

    public EpochCurrentRows getNextEpochsState(int hours) {
        int leftHours = Math.min(hours, getAvailableLearningSetSize());
        log.info("leftHour = {}, hours = {}", leftHours, hours);

        List<EpochCurrentRows.Row> newRows = IntStream.range(0, leftHours)
                .mapToObj(x -> marketLearningListener.nextHour(currentAgentId))
                .collect(Collectors.toList());

        if (leftHours < hours) {
            epochEnded = true;
            agentService.saveEpochResult(currentAgentId);
        }
        return new EpochCurrentRows()
                .setValues(newRows)
                .setAgentId(currentAgentId);
    }

    private int getAvailableLearningSetSize() {
        return (int) ((assetLearningChartRepository.getLastLearningTime() -
                currentEpochService.getCurrentEpochTime()) / 1000 / 60 / 60);
    }

    public Boolean goNextEpoch() {
        agentService.nextEpochCurrentAgent(currentAgentId);
        epochEnded = false;
        marketLearningListener.setSteps(0);
        marketLearningListener.getEpochRows().clear();
        return true;
    }

    @Async
    public void goFullEpoch() {
        getNextEpochsState(getAvailableLearningSetSize());
    }

    public long getLearnProgress() {
        int availableLearningSetSize = getAvailableLearningSetSize();
        log.info("full size = {}, available size = {}", fullSize, availableLearningSetSize);
        return 100 - (availableLearningSetSize * 100) / fullSize;
    }
}
