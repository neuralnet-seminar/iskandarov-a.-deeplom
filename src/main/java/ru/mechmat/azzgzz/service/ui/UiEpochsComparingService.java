package ru.mechmat.azzgzz.service.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.entity.ui.EpochResult;
import ru.mechmat.azzgzz.repository.learning.AssetLearningChartRepository;
import ru.mechmat.azzgzz.repository.learning.EpochHistoryRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UiEpochsComparingService {

    private final EpochHistoryRepository epochHistoryRepository;
    private final AssetLearningChartRepository assetLearningChartRepository;

    public List<EpochResult> getEpochComparingChart(String agentId) {
        List<EpochResult> epochResults = new ArrayList<>(11);
        BigDecimal assetLearnDelta = assetLearningChartRepository.getAssetLearnDelta();
        epochResults.add(new EpochResult()
                .setAgentId("Asset")
                .setDelta(assetLearnDelta)
        );
        epochResults.addAll(epochHistoryRepository.getEpochResults(agentId));
        return epochResults;
    }
}
