package ru.mechmat.azzgzz.service;

import com.binance.api.client.domain.OrderStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.client.BinanceApiPrettyRestClient;
import ru.mechmat.azzgzz.client.ReinforcementAgent;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.dto.AgentDecideRequest;
import ru.mechmat.azzgzz.dto.AgentLearnRequest;
import ru.mechmat.azzgzz.entity.learning.CurrentEpochRow;
import ru.mechmat.azzgzz.entity.market.NumericAccount;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.entity.market.NumericNewOrder;
import ru.mechmat.azzgzz.entity.market.NumericNewOrderResponse;
import ru.mechmat.azzgzz.entity.ui.EpochCurrentRows;
import ru.mechmat.azzgzz.repository.learning.AgentLearningAccountRepository;
import ru.mechmat.azzgzz.service.learning.CurrentEpochService;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ru.mechmat.azzgzz.dictionary.Ticker.ETHUSDT;
import static ru.mechmat.azzgzz.service.TradingProcessService.HOUR_TIME_STEP;
import static ru.mechmat.azzgzz.util.AccountUtils.getAssetQty;
import static ru.mechmat.azzgzz.util.AccountUtils.getVolume;

/**
 * Класс имитирующий листенер Биржи. Если реальный листенер будет каждую минуту опрашивать биржу
 * и сохранять данные в инкаминги, то обучающий листенер будет по "нажатию кнопки" вычитывать
 * инкаминги.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MarketLearningListener {

    private final CurrentEpochService currentEpochService;
    private final AgentDecisionTranslator agentDecisionTranslator;
    private final AnalyticService analyticService;
    private final BinanceApiPrettyRestClient binanceApiPrettyRestClient;
    private final AgentLearningAccountRepository agentLearningAccountRepository;
    private final ReinforcementAgent reinforcementAgent;

    private BigDecimal previousHourAssetCloseValue = null;

    @Getter
    private final List<CurrentEpochRow> epochRows = new ArrayList<>(20000);

    private final BigDecimal targetSigma = new BigDecimal(15);

    private List<BigDecimal> weights;

    @Getter
    @Setter
    private int steps = 0;


    @PostConstruct
    public void calcDeviationWeights() {
        BigDecimal weightsSum = BigDecimal.valueOf(
                IntStream.range(0, 59)
                        .mapToDouble(x -> Math.pow(2, -x))
                        .sum());
        weights = IntStream.range(0, 59)
                .mapToObj(x -> BigDecimal.valueOf(Math.pow(2, -x)))
                .map(x -> x.divide(weightsSum, 8, RoundingMode.HALF_UP))
                .collect(Collectors.toList());
    }

    public EpochCurrentRows.Row nextHour(String agentId) {
        //TODO начало эпохи обучения начинается в 01:00, т.к. до этого агент не принимает решений
        NumericAccount account = binanceApiPrettyRestClient.getAccount();
        long timeToGetData = binanceApiPrettyRestClient.getServerTime();
        CurrentEpochRow currentRow = currentEpochService.getRow(timeToGetData - HOUR_TIME_STEP);
        epochRows.add(currentRow);

        if (steps % 24 == 0 && steps > 0) {
            reinforcementAgent.learn(new AgentLearnRequest()
                    .setReward(getReward(agentId, epochRows))
            );
        }

        List<NumericCandlestick> candlestickBars = binanceApiPrettyRestClient.get60PreviousMinutesBars(ETHUSDT, timeToGetData);

        AgentDecideRequest decideRequest = Optional.of(candlestickBars)
                .map(this::createGraphic)
                .map(x -> x.setCoinPart(getCoinPart(ETHUSDT, currentRow)))
                .map(x -> fillCoinHourDelta(x, candlestickBars))
                .map(x -> fillPortfolioHourDelta(x, candlestickBars, ETHUSDT, account))
                .map(request -> analyticService.enrichDecideRequest(request, epochRows, steps))
                .get();

        BigDecimal actionId = reinforcementAgent.decide(decideRequest);

        if (BigDecimal.ZERO.compareTo(actionId) != 0) {
            NumericNewOrder newOrder = agentDecisionTranslator.translate(
                    actionId, account, candlestickBars.get(candlestickBars.size() - 1).getClose(), ETHUSDT);
            NumericNewOrderResponse numericNewOrderResponse = binanceApiPrettyRestClient.newOrder(newOrder);
            if (!OrderStatus.FILLED.equals(numericNewOrderResponse.getStatus())) {
                addDuplicateRowToAgentLearningAccount();
            }
        } else {
            addDuplicateRowToAgentLearningAccount();
        }
        steps++;

        return updateCurrentEpoch(agentId, timeToGetData);
    }

    private BigDecimal getReward(String agentId, List<CurrentEpochRow> epochRows) {
        BigDecimal row1 = epochRows.get(steps).getAgentValue();
        BigDecimal row2 = epochRows.get(Math.max(0, steps - 24)).getAgentValue();
        BigDecimal estimatedVolatility = calcWeightedDeviation(epochRows);

        log.info("sigma_t = {}", estimatedVolatility);
        return targetSigma.divide(estimatedVolatility, 8, RoundingMode.HALF_UP).multiply(row1.divide(row2, 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE)).subtract(BigDecimal.valueOf(0.001));
    }

    private BigDecimal calcWeightedDeviation(List<CurrentEpochRow> epochRows) {
        List<BigDecimal> observations = new ArrayList<>(60);
        int size = epochRows.size();
        for (int i = 0; i * 24 < size && i < 60; i++) {
            observations.add(epochRows.get(size - 1 - i * 24).getAssetValue());
        }
        BigDecimal avg = calculateWeightedAverage(observations);
        BigDecimal deviationSum = calcDeviationSum(observations, avg);
        return BigDecimal.valueOf(Math.sqrt(deviationSum.multiply(BigDecimal.valueOf(60 / 59)).doubleValue()));
    }

    private BigDecimal calculateWeightedAverage(List<BigDecimal> observations) {
        BigDecimal sum = BigDecimal.ZERO;
        for (int i = 0; i < observations.size(); i++) {
            sum = sum.add(observations.get(i).multiply(weights.get(i)));
        }
        return sum;
    }

    private BigDecimal calcDeviationSum(List<BigDecimal> observations, BigDecimal avg) {
        BigDecimal sum = BigDecimal.ZERO;
        for (int i = 0; i < observations.size(); i++) {
            sum = sum.add(observations.get(i).multiply(weights.get(i)));
        }
        return sum;
    }

    private BigDecimal calcSingleDeviation(BigDecimal x, BigDecimal avg) {
        return x.subtract(avg).pow(2);
    }

    private void addDuplicateRowToAgentLearningAccount() {
        agentLearningAccountRepository.duplicateLastRowWithNextHour();
    }

    private EpochCurrentRows.Row updateCurrentEpoch(String agentId, long time) {
        NumericAccount account = binanceApiPrettyRestClient.getAccount();
        BigDecimal volume = getVolume(account, previousHourAssetCloseValue, ETHUSDT);
        EpochCurrentRows.Row row = new EpochCurrentRows.Row()
                .setTime(time)
                .setAssetRatio(getCoinPart(ETHUSDT, account))
                .setAssetValue(previousHourAssetCloseValue)
                .setAgentValue(volume);
        currentEpochService.addCurrentRow(agentId, row);
        return row;
    }

    private BigDecimal getActionId() {
        BigDecimal bigDecimal = BigDecimal.valueOf(Math.random()).subtract(new BigDecimal("0.5"));
        if (bigDecimal.compareTo(BigDecimal.valueOf(-0.02)) > 0 && bigDecimal.compareTo(BigDecimal.valueOf(0.02)) < 0) {
            return BigDecimal.ZERO;
        }
        return bigDecimal;
    }

    private AgentDecideRequest createGraphic(List<NumericCandlestick> candlestickBars) {
        AgentDecideRequest decideRequest = new AgentDecideRequest();
        List<List<BigDecimal>> graphic = new ArrayList<>();
        NumericCandlestick numericCandlestick0 = candlestickBars.get(0);
        graphic.add(Arrays.asList(
                divide(numericCandlestick0.getOpen(), previousHourAssetCloseValue),
                divide(numericCandlestick0.getHigh(), numericCandlestick0.getOpen()),
                divide(numericCandlestick0.getLow(), numericCandlestick0.getOpen()),
                divide(numericCandlestick0.getClose(), numericCandlestick0.getOpen())
        ));

        previousHourAssetCloseValue = numericCandlestick0.getClose();

        for (int i = 1; i < candlestickBars.size(); i++) {
            NumericCandlestick numericCandlestick = candlestickBars.get(i);
            graphic.add(Arrays.asList(
                    divide(numericCandlestick.getOpen(), previousHourAssetCloseValue),
                    divide(numericCandlestick.getHigh(), numericCandlestick.getOpen()),
                    divide(numericCandlestick.getLow(), numericCandlestick.getOpen()),
                    divide(numericCandlestick.getClose(), numericCandlestick.getOpen())
            ));
            previousHourAssetCloseValue = candlestickBars.get(i).getClose();
        }
        return decideRequest.setPriceDeltaLastHour(graphic);
    }


    private BigDecimal getCoinPart(Ticker ticker, CurrentEpochRow currentEpochRow) {
        return currentEpochRow.getAssetRatio().setScale(8, RoundingMode.HALF_UP);
    }

    private BigDecimal getCoinPart(Ticker ticker, NumericAccount account) {
        BigDecimal coinQty = getAssetQty(account, ticker.getBaseAsset());
        BigDecimal cash = getAssetQty(account, ticker.getQuoteAsset());
        BigDecimal price = previousHourAssetCloseValue;
        BigDecimal coinVolume = coinQty.multiply(price);
        return coinVolume.divide((coinVolume.add(cash)), 8, RoundingMode.HALF_UP);
    }

    private AgentDecideRequest fillCoinHourDelta(AgentDecideRequest request, List<NumericCandlestick> candlesticks) {
        BigDecimal first = candlesticks.get(0).getOpen();
        BigDecimal last = candlesticks.get(candlesticks.size() - 1).getClose();
        return request.setCoinHourDelta(last.divide(first, 8, BigDecimal.ROUND_HALF_UP).subtract(BigDecimal.ONE));
    }

    private AgentDecideRequest fillPortfolioHourDelta(AgentDecideRequest request,
                                                      List<NumericCandlestick> candlestickBars,
                                                      Ticker ticker, NumericAccount account) {
        BigDecimal volumePrev = getVolume(account, previousHourAssetCloseValue, ticker);
        BigDecimal volumeLast = getVolume(account, candlestickBars.get(candlestickBars.size() - 1).getClose(), ticker);
        return request.setPortfolioHourDelta(
                volumeLast.divide(volumePrev, 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE)
        );
    }


    private BigDecimal divide(BigDecimal current, BigDecimal previous) {
        if (previous == null || BigDecimal.ZERO.compareTo(previous) == 0) {
            return BigDecimal.ZERO;
        }
        return current.divide(previous, RoundingMode.HALF_UP).setScale(8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE);
    }
}
