package ru.mechmat.azzgzz.service;

import com.binance.api.client.domain.account.NewOrder;
import org.springframework.stereotype.Component;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericAccount;
import ru.mechmat.azzgzz.entity.market.NumericNewOrder;
import ru.mechmat.azzgzz.util.AccountUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static ru.mechmat.azzgzz.util.AccountUtils.getAssetQty;
import static ru.mechmat.azzgzz.util.AccountUtils.getVolume;

@Component
public class AgentDecisionTranslator {

    public NumericNewOrder translate(BigDecimal actionId, NumericAccount account, BigDecimal price, Ticker ticker) {
        BigDecimal assetQty = getAssetQty(account, ticker.getBaseAsset());
        BigDecimal currentAssetVolume = assetQty.multiply(price);
        BigDecimal totalVolume = getVolume(account, price, ticker);
        BigDecimal currentRatio = currentAssetVolume.divide(totalVolume, 8, RoundingMode.HALF_UP);

        BigDecimal needVolumeDelta;

        if (actionId.compareTo(BigDecimal.ZERO) > 0) {
            if (currentRatio.add(actionId).compareTo(BigDecimal.ONE) < 0) {
                needVolumeDelta = actionId;
            } else {
                needVolumeDelta = BigDecimal.ONE.subtract(currentRatio);
            }
            BigDecimal needDelta = needVolumeDelta
                    .multiply(totalVolume)
                    .divide(price, 8, RoundingMode.HALF_UP)
                    .multiply(BigDecimal.valueOf(0.99))
                    .setScale(5, RoundingMode.CEILING)
                    .setScale(8, RoundingMode.HALF_UP);
            return NumericNewOrder.marketBuy(ticker, needDelta);

        } else {
            if (currentRatio.add(actionId).compareTo(BigDecimal.ZERO) > 0) {
                return NumericNewOrder.marketSell(ticker, actionId
                        .multiply(BigDecimal.valueOf(-1))
                        .multiply(totalVolume)
                        .divide(price, 8, RoundingMode.HALF_UP)
                        .multiply(BigDecimal.valueOf(0.99))
                        .setScale(5, RoundingMode.CEILING)
                        .setScale(8, RoundingMode.HALF_UP));
            } else {
                needVolumeDelta = assetQty;
            }
            return NumericNewOrder.marketSell(ticker, needVolumeDelta);
        }
    }
}
