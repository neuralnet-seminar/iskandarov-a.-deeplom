package ru.mechmat.azzgzz.service;

import com.binance.api.client.domain.market.Candlestick;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.repository.IncomingRepository;
import ru.mechmat.azzgzz.converter.CandlestickConverter;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class IncomingService {

    private final IncomingRepository incomingRepository;

    public List<Candlestick> get60CandlesticksFromTime(Ticker ticker, long openTime) {
        return mapToBinance(incomingRepository.get60CandlesticksFromTime(ticker, openTime));
    }

    private List<Candlestick> mapToBinance(List<NumericCandlestick> candlesticksFromTime) {
        return candlesticksFromTime.stream()
                .map(CandlestickConverter::convert)
                .collect(Collectors.toList());
    }


}
