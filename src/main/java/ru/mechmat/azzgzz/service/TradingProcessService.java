package ru.mechmat.azzgzz.service;

import com.binance.api.client.domain.account.NewOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.client.BinanceApiPrettyRestClient;
import ru.mechmat.azzgzz.client.ReinforcementAgent;
import ru.mechmat.azzgzz.dictionary.Ticker;
import ru.mechmat.azzgzz.dto.AgentDecideRequest;
import ru.mechmat.azzgzz.entity.market.NumericAccount;
import ru.mechmat.azzgzz.entity.market.NumericCandlestick;
import ru.mechmat.azzgzz.repository.impl.AccountRepository;
import ru.mechmat.azzgzz.util.TimeUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.mechmat.azzgzz.util.AccountUtils.getAssetQty;
import static ru.mechmat.azzgzz.util.AccountUtils.getVolume;

@Slf4j
@Service
@RequiredArgsConstructor
public class TradingProcessService {

    public static final int HOUR_TIME_STEP = 3_600_000;

    private final BinanceApiPrettyRestClient binanceApiRestClient;
    private final ReinforcementAgent reinforcementAgent;
    private final AccountRepository accountRepository;
    private final AgentDecisionTranslator agentDecisionTranslator;

    private volatile long currentTime;
    private volatile NumericAccount lastAccount;
    private volatile NumericAccount currentAccount;

    public void init() {
        reinforcementAgent.init(); //TODO передавать agent_id
        currentTime = TimeUtils.truncate(binanceApiRestClient.getServerTime(), ChronoUnit.MINUTES);
        lastAccount = binanceApiRestClient.getAccount();
        currentAccount = binanceApiRestClient.getAccount();
//        saveAccountInfo(lastAccount, currentTime - HOUR_TIME_STEP);
//        saveAccountInfo(currentAccount, currentTime);
    }


    public void oneHour() {
//        List<NumericCandlestick> candlestickBars = binanceApiRestClient.get60PreviousMinutesBars(
//                Ticker.ETHUSDT, currentTime);
//        AgentDecideRequest decideRequest = Optional.of(candlestickBars)
//                .map(this::createGraphic)
//                .map(x -> fillCoinPart(x, candlestickBars, Ticker.ETHUSDT))
//                .map(x -> fillCoinHourDelta(x, candlestickBars))
//                .map(x -> fillPortfolioHourDelta(x, candlestickBars, Ticker.ETHUSDT))
//                .get();
//        BigDecimal actionId = reinforcementAgent.decide(decideRequest);
//        NewOrder newOrder = agentDecisionTranslator.translate(
//                actionId, currentAccount, candlestickBars.get(candlestickBars.size() - 1).getClose(), Ticker.ETHUSDT);
//
////        binanceApiRestClient.newOrder(newOrder);
//        currentTime += HOUR_TIME_STEP;
    }
//
//    private AgentDecideRequest createGraphic(List<NumericCandlestick> candlestickBars) {
//        AgentDecideRequest decideRequest = new AgentDecideRequest();
//        List<List<BigDecimal>> graphic = new ArrayList<>();
//        graphic.add(Arrays.asList(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE));
//
//        for (int i = 1; i < candlestickBars.size(); i++) {
//            NumericCandlestick previousCandlestick = candlestickBars.get(i - 1);
//            NumericCandlestick currentCandlestick = candlestickBars.get(i);
//            graphic.add(Arrays.asList(
//                    divide(previousCandlestick.getOpen(), currentCandlestick.getOpen()),
//                    divide(previousCandlestick.getHigh(), currentCandlestick.getHigh()),
//                    divide(previousCandlestick.getLow(), currentCandlestick.getLow()),
//                    divide(previousCandlestick.getClose(), currentCandlestick.getClose())
//            ));
//        }
//
//        return decideRequest.setPriceDeltaLastHour(graphic);
//    }
//
//    private AgentDecideRequest fillCoinPart(AgentDecideRequest request, List<NumericCandlestick> candlesticks, Ticker ticker) {
//        BigDecimal coinQty = getAssetQty(currentAccount, ticker.getBaseAsset());
//        BigDecimal cash = getAssetQty(currentAccount, ticker.getQuoteAsset());
//        BigDecimal price = candlesticks.get(candlesticks.size() - 1).getClose();
//        BigDecimal coinVolume = coinQty.multiply(price);
//        return request.setCoinPart(
//                coinVolume.divide((coinVolume.add(cash)), 8, RoundingMode.HALF_UP)
//        );
//    }
//
//    private AgentDecideRequest fillCoinHourDelta(AgentDecideRequest request, List<NumericCandlestick> candlesticks) {
//        BigDecimal first = candlesticks.get(0).getOpen();
//        BigDecimal last = candlesticks.get(candlesticks.size() - 1).getClose();
//        return request.setCoinHourDelta(last.divide(first, 8, BigDecimal.ROUND_HALF_UP));
//    }
//
//    private AgentDecideRequest fillPortfolioHourDelta(AgentDecideRequest request, List<NumericCandlestick> candlestickBars, Ticker ticker) {
//        BigDecimal volumePrev = getVolume(lastAccount, candlestickBars.get(0).getClose(), ticker);
//        BigDecimal volumeLast = getVolume(currentAccount, candlestickBars.get(candlestickBars.size() - 1).getClose(), ticker);
//        return request.setCoinHourDelta(
//                volumeLast.divide(volumePrev, 8, RoundingMode.HALF_UP)
//        );
//    }
//
//    private BigDecimal divide(BigDecimal previous, BigDecimal current) {
//        return current.divide(previous, RoundingMode.HALF_UP).setScale(8, RoundingMode.HALF_UP);
//    }
//
//    private void saveAccountInfo(NumericAccount account, Long time) {
//        accountRepository.saveAccountInfo(account.getBalances()
//                .stream()
//                .map(x -> x.setTime(time))
//                .collect(Collectors.toList())
//        );
//    }
}
