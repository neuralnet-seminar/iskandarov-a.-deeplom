package ru.mechmat.azzgzz.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.dto.AgentDecideRequest;
import ru.mechmat.azzgzz.entity.learning.CurrentEpochRow;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.lang.Math.max;

@Service
@RequiredArgsConstructor
public class AnalyticService {

    public AgentDecideRequest enrichDecideRequest(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        enrichDayDelta(request, epochRows, steps);
        enrichWeekDelta(request, epochRows, steps);
        enrichMonthDelta(request, epochRows, steps);
        enrich3MonthDelta(request, epochRows, steps);
        enrich6MonthDelta(request, epochRows, steps);
        enrichMDrowdown(request, epochRows, steps);
        enrichDeviation(request, epochRows, steps);
        return request;
    }

    private void enrichDayDelta(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        CurrentEpochRow atWeekStart = epochRows.get(max(0, steps - 24));
        CurrentEpochRow atWeekEnds = epochRows.get(steps);
        request.setCoinDDelta(atWeekEnds.getAssetValue().divide(atWeekStart.getAssetValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
        request.setPortfolioDDelta(atWeekEnds.getAgentValue().divide(atWeekStart.getAgentValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
    }

    private void enrichWeekDelta(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        CurrentEpochRow atWeekStart = epochRows.get(max(0, steps - 7 * 24));
        CurrentEpochRow atWeekEnds = epochRows.get(steps);
        request.setCoinWDelta(atWeekEnds.getAssetValue().divide(atWeekStart.getAssetValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
        request.setPortfolioWDelta(atWeekEnds.getAgentValue().divide(atWeekStart.getAgentValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
    }

    private void enrichMonthDelta(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        CurrentEpochRow atWeekStart = epochRows.get(max(0, steps - 30 * 24));
        CurrentEpochRow atWeekEnds = epochRows.get(steps);
        request.setCoinMDelta(atWeekEnds.getAssetValue().divide(atWeekStart.getAssetValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
        request.setPortfolioMDelta(atWeekEnds.getAgentValue().divide(atWeekStart.getAgentValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
    }

    private void enrich3MonthDelta(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        CurrentEpochRow atPeriodStart = epochRows.get(max(0, steps - 30 * 24 * 3));
        CurrentEpochRow atPeriodEnds = epochRows.get(steps);
        request.setCoin3MDelta(atPeriodEnds.getAssetValue().divide(atPeriodStart.getAssetValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
    }

    private void enrich6MonthDelta(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        CurrentEpochRow atWeekStart = epochRows.get(max(0, steps - 30 * 24 * 6));
        CurrentEpochRow atWeekEnds = epochRows.get(steps);
        request.setCoin6MDelta(atWeekEnds.getAssetValue().divide(atWeekStart.getAssetValue(), 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));
    }

    private void enrichMDrowdown(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        BigDecimal maxAssetValue = epochRows.get(0).getAssetValue();
        for (int i = max(0, steps - 30 * 24); i < steps; i++) {
            CurrentEpochRow currentEpochRow = epochRows.get(i);
            if (maxAssetValue.compareTo(currentEpochRow.getAssetValue()) <= 0) {
                maxAssetValue = currentEpochRow.getAssetValue();
            }
        }
        request.setMDrowdown(BigDecimal.ONE
                .subtract(epochRows.get(steps).getAssetValue().divide(maxAssetValue, 8, RoundingMode.HALF_UP))
        );
    }

    /**
     * Отклонение от среднего за 30 дней
     */
    private void enrichDeviation(AgentDecideRequest request, List<CurrentEpochRow> epochRows, int steps) {
        int skipSize = max(0, steps - 24 * 30);
        int periodSize = steps - skipSize;
        periodSize = periodSize <= 0? 1 : periodSize;
        BigDecimal mean = epochRows.stream()
                .skip(skipSize)
                .map(CurrentEpochRow::getAssetValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(periodSize), 8, RoundingMode.HALF_UP);
        request.setDeviationLine(epochRows.get(steps).getAssetValue()
                .divide(mean, 8, RoundingMode.HALF_UP
                ).subtract(BigDecimal.ONE)
        );
    }

}
