package ru.mechmat.azzgzz.service.learning;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.entity.learning.CurrentEpochRow;
import ru.mechmat.azzgzz.entity.ui.EpochCurrentRows;
import ru.mechmat.azzgzz.repository.learning.CurrentEpochRepository;

import java.math.BigDecimal;

@Slf4j
@Service
@RequiredArgsConstructor
public class CurrentEpochService {

    private final CurrentEpochRepository currentEpochRepository;

    public EpochCurrentRows getCurrentEpochAllRows() {
        return currentEpochRepository.getAllRowsOfCurrentEpoch();
    }

    public Long getCurrentEpochTime() {
        return currentEpochRepository.getCurrentTime();
    }

    public int addCurrentRow(String agentId, EpochCurrentRows.Row row) {
        return currentEpochRepository.insertCurrentEpochRow(agentId, row);
    }

    public BigDecimal getAgentValue(String agentId, long time) {
        return currentEpochRepository.getAgentValue(agentId, time);
    }

    public CurrentEpochRow getRow(long time) {
        return currentEpochRepository.getRow(time);
    }
}
