package ru.mechmat.azzgzz.service.learning;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.mechmat.azzgzz.client.ReinforcementAgent;
import ru.mechmat.azzgzz.entity.market.NumericAssetBalance;
import ru.mechmat.azzgzz.entity.ui.EpochCurrentRows;
import ru.mechmat.azzgzz.entity.ui.EpochResult;
import ru.mechmat.azzgzz.repository.learning.AgentLearningAccountRepository;
import ru.mechmat.azzgzz.repository.learning.AssetLearningChartRepository;
import ru.mechmat.azzgzz.repository.learning.CurrentEpochRepository;
import ru.mechmat.azzgzz.repository.learning.EpochHistoryRepository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static ru.mechmat.azzgzz.dictionary.Ticker.ETHUSDT;

@Slf4j
@Service
@RequiredArgsConstructor
public class AgentService {

    public static final BigDecimal INITIAL_AGENT_VALUE = BigDecimal.valueOf(100);
    private final CurrentEpochRepository currentEpochRepository;
    private final AssetLearningChartRepository assetLearningChartRepository;
    private final AgentLearningAccountRepository agentLearningAccountRepository;
    private final EpochHistoryRepository epochHistoryRepository;

    private final ReinforcementAgent reinforcementAgent;

    private int epochNumber;

    private EpochCurrentRows.Row createFirstEpochState() {
        Long firstLearningTime = assetLearningChartRepository.getFirstLearningTime();
        return new EpochCurrentRows.Row()
                .setTime(firstLearningTime)
                .setAssetRatio(BigDecimal.ZERO)
                .setAssetValue(assetLearningChartRepository.getLearnRow(firstLearningTime).getValue())
                .setAgentValue(INITIAL_AGENT_VALUE);
    }

    public Boolean createAgent(String agentId) {
        reinforcementAgent.init();
        epochHistoryRepository.deleteAllAboutAgent(agentId);
        epochNumber = 1;
        insertEpochResultRowWithoutValues(agentId);
        clearPreviousAndSetNewEpoch(agentId, createFirstEpochState());
        return true;
    }

    public boolean nextEpochCurrentAgent(String agentId) {
        epochNumber++;
        insertEpochResultRowWithoutValues(agentId);
        clearPreviousAndSetNewEpoch(agentId, createFirstEpochState());
        return true;
    }

    public void prepareForTest(String agentId) {
        Long firstTestTime = assetLearningChartRepository.getFirstTestTime();

        EpochCurrentRows.Row firstEpochState = new EpochCurrentRows.Row()
                .setTime(firstTestTime)
                .setAssetRatio(BigDecimal.ZERO)
                .setAssetValue(assetLearningChartRepository.getTestRow(firstTestTime).getValue())
                .setAgentValue(INITIAL_AGENT_VALUE);
        clearPreviousAndSetNewEpoch(agentId, firstEpochState);
    }

    private void insertEpochResultRowWithoutValues(String agentId) {
        epochHistoryRepository.insertEpochResult(new EpochResult()
                .setAgentId(agentId)
                .setEpochId(epochNumber)
        );
    }

    public void saveEpochResult(String agentId) {
        epochHistoryRepository.updateEpochResult(new EpochResult()
                .setAgentId(agentId)
                .setEpochId(epochNumber)
                .setStartValue(currentEpochRepository.getStartAgentValue())
                .setEndValue(currentEpochRepository.getEndAgentValue())
        );
    }

    private void clearPreviousAndSetNewEpoch(String agentId, EpochCurrentRows.Row firstEpochState) {
        currentEpochRepository.deleteEpochData();
        currentEpochRepository.insertCurrentEpochRow(agentId, firstEpochState);
        agentLearningAccountRepository.deleteAccountData();
        agentLearningAccountRepository.insertAccount(getAssetBalanceList(agentId, firstEpochState));
    }


    private List<NumericAssetBalance> getAssetBalanceList(String agentId, EpochCurrentRows.Row firstEpochState) {
        return Arrays.asList(
                new NumericAssetBalance()
                        .setTime(firstEpochState.getTime())
                        .setAsset(ETHUSDT.getBaseAsset())
                        .setFree(BigDecimal.ZERO)
                        .setAgentId(agentId),
                new NumericAssetBalance()
                        .setTime(firstEpochState.getTime())
                        .setAsset(ETHUSDT.getQuoteAsset())
                        .setFree(INITIAL_AGENT_VALUE)
                        .setAgentId(agentId)
        );
    }
}
