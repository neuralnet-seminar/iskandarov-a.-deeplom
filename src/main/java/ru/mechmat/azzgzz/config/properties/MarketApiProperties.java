package ru.mechmat.azzgzz.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("api.binance")
public class MarketApiProperties {

    private String mode;
}
