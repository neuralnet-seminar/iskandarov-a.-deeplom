package ru.mechmat.azzgzz.config.db;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

import static ru.mechmat.azzgzz.config.db.DatabaseBeans.*;

@Configuration
public class DeeplomDbConfig {

    public static final String PREFIX = "sql";
    public static final String PROPERTIES_PREFIX = "datasource." + PREFIX;
    public static final String PROPERTIES_HIKARI_PREFIX = PROPERTIES_PREFIX + ".hikari";

    public static final String DATA_SOURCE_PROPERTIES = PREFIX + DATA_SOURCE_PROPERTIES_BEAN;
    public static final String DATA_SOURCE = PREFIX + DATA_SOURCE_BEAN;
    public static final String JDBC_TEMPLATE = PREFIX + JDBC_TEMPLATE_BEAN;
    public static final String TX_MANAGER = PREFIX + TX_MANAGER_BEAN;

    @Primary
    @Bean(DATA_SOURCE_PROPERTIES)
    @ConfigurationProperties(PROPERTIES_PREFIX)
    public DataSourceProperties dataSourceProperties(){
        return new DataSourceProperties();
    }

    @Bean(DATA_SOURCE)
    @ConfigurationProperties(PROPERTIES_HIKARI_PREFIX)
    public DataSource dataSource(@Qualifier(DATA_SOURCE_PROPERTIES) DataSourceProperties properties) {
        return properties.initializeDataSourceBuilder().build();
    }

    @Bean(JDBC_TEMPLATE)
    public JdbcTemplate jdbcTemplate (@Qualifier(DATA_SOURCE) DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    @Bean(TX_MANAGER)
    public DataSourceTransactionManager dataSourceTransactionManager(@Qualifier(DATA_SOURCE) DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
