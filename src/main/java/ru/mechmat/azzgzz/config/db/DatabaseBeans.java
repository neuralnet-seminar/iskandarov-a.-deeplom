package ru.mechmat.azzgzz.config.db;

public class DatabaseBeans {

    public static final String DATA_SOURCE_PROPERTIES_BEAN = "DataSourceProperties";
    public static final String DATA_SOURCE_BEAN = "DataSource";
    public static final String JDBC_TEMPLATE_BEAN = "JdbcTemplate";
    public static final String TX_MANAGER_BEAN = "TxManager";

}
