package ru.mechmat.azzgzz.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.mechmat.azzgzz.config.properties.MarketApiProperties;

@Configuration
@EnableConfigurationProperties(value = {
        MarketApiProperties.class
})
@EnableScheduling
@EnableAsync
public class ApplicationConfig {
}
