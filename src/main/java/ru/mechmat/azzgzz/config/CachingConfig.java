package ru.mechmat.azzgzz.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;

@EnableCaching
public class CachingConfig {

    @Bean
    CacheManager cacheManager() {
        return new CaffeineCacheManager();
    }
}
