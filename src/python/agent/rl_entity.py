# Агенту будет доступно 20 действий, 0 <= id < 20. 
# "buy 25%" означает увеличить долю актива в портфеле на 25 п.п. 
#     (пример: 10% -> 35%, 90% -> 100%)
# "sell 50%" - снизить долю актива в портфеле на 50 п.п. 
#     (пример: 80% -> 30%, 45% -> 0%)

from decimal import Decimal

class Action(object):
    
    __actions = ["buy 15%", "buy 10%", "buy 7%", "buy 5%",
                 "buy 3%", "buy 2%", "buy 1%", "wait", 
                 "sell 1%", "sell 2%", "sell 3%", "sell 5%", "sell 7%", "sell 10%",
                 "sell 15%"]
    
    _values = [15, 10, 7, 5, 3, 2, 1, 0, -1, -2, -3, -5, -7, -10, -15]
    
    def __init__(self, x):
        assert x < len(self._values)
        self.id = x
    
    def name(self):
        return self.__actions[self.id]
    
    def value(self):
        return round(float(self._values[self.id])/100, 2)
    
    def serialize(self):
        return { "action_id": float(self.value())}

    
    
class State(object):
    
    def __init__(self):
        self._coin_price_last_hour = [] # матрица 60 х 4 (60 минутных свечей)
        self.price_delta_last_hour = [] # матрица 60 х 4 - отношение a[i][j]/a[i-1][j]
        self._cash = 0 
        self._coins = 0 # количество монет
        self.coin_part = 0 # доля стоимости монет в ценности портфеля 
        self.coin_hour_delta = 0 # a[60][4](now) / a[60][4](now - 1m)
        self.portfolio_hour_delta = 0
        # analytics
        self.coin_d_delta = 0
        self.coin_w_delta = 0
        self.coin_m_delta = 0
        self.coin_3m_delta = 0
        self.coin_6m_delta = 0
        self.coin_y_delta = 0
        self.portfolio_d_delta = 0
        self.portfolio_w_delta = 0
        self.portfolio_m_delta = 0
        self.deviation_exp = 0 # "свежие" свечи имеют больший вес
        self.deviation_line = 0 # отклонение от среднего за 30 дней
        self.volatility_exp = 0
        self.volatility_line = 0 # волатильность за 30 дней
        self.m_drowdown = 0 # текущая просадка от максимума за 30 дней
    
    def to_list(self):
        a = []
        for i in range(60):
            for j in range(4):
                a.append(self.price_delta_last_hour[i][j])
        a.append(self.coin_part)
        a.append(self.coin_hour_delta)
        a.append(self.portfolio_hour_delta)
        a.append(self.coin_d_delta)
        a.append(self.coin_w_delta)
        a.append(self.coin_m_delta)
        a.append(self.coin_3m_delta)
        a.append(self.coin_6m_delta)
        a.append(self.coin_y_delta)
        a.append(self.portfolio_d_delta)
        a.append(self.portfolio_w_delta)
        a.append(self.portfolio_m_delta)
        a.append(self.deviation_exp)
        a.append(self.deviation_line)
        a.append(self.volatility_exp)
        a.append(self.volatility_line)
        a.append(self.m_drowdown)
        return a
    
    def __repr__(self):
        return """State (cash: %.2f $, 
    coins: %.2f $, 
    coin_price_delta: %.2f,
    portfolio_hour_delta: %.2f,
    portfolio_d_delta: %.2f)""" % (
            self._cash, 
            self._coins,
            self.coin_hour_delta,
            self.portfolio_hour_delta,
            self.portfolio_d_delta)
    
    def serialize(self):
        return {
            "price_delta_last_hour": self.price_delta_last_hour,
            "cash": self._cash,
            "coins": self._coins
        }