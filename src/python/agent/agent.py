from keras.models import Sequential, model_from_json
from keras.layers import Dense
from keras.utils import np_utils
from keras.callbacks import History
import numpy as np
import rl_entity as re
import tensorflow as tf

import random
from datetime import datetime
random.seed(datetime.now())




# properties
enough_reward = 0.01 # - Увеличение ценности портфеля не менее, чем на 1% в день
start_risk = 0.2
final_risk = 0.05
gamma = 0.99         # - Коэф. дисконтирования
alpha = 1          # - Скорость обучения

stepsToRandom = 24*365*5. # количество шагов до того, как рандом станет минимальным

def random_choise_chance(step) :
    return start_risk * max(0., float(stepsToRandom - step))/stepsToRandom + final_risk





class Agent(object):

    def __init__(self, state_example, actions):

        self._step = 0
        self.state_history = []
        self.action_history = []
        self.prediction_history = []
        self.actions_pool = actions
        self.__init_newral_net(state_example, actions)
        global graph
        graph = tf.get_default_graph()



    def __init_newral_net(self, state_exapmle, actions):
        tf.TF_CPP_MIN_VLOG_LEVEL=2
        solutor = Sequential()
        solutor.add(Dense(32,                           # количество нейронов на первом слое
                          kernel_initializer="normal", # норм. распр. начальных весов
                          input_dim = len(state_exapmle.to_list()),
                          activation="relu"))          # функция активации: max(0,x)
        solutor.add(Dense(24,
                          kernel_initializer="normal",
                          activation="relu"))
        solutor.add(Dense(len(actions)))
        solutor.compile(loss = "mean_squared_error", # т.к. сеть классифицирует
                        optimizer='SGD'               # стохаст. град. спуск
                        #metrics=['accuracy']          # точность
                        )
        self.solutor = solutor



    # ожидается, что в state придет одномерный лист
    def decide(self, state):
        state = state.to_list()
        rand_max = random_choise_chance(self._step)
        with graph.as_default():
            prediction = self.solutor.predict(np.array([state], float))
        if random.random() > rand_max:
            solution = prediction
        else:
            solution = [random.random() for i in range(len(self.actions_pool))]

        print(prediction)
        self._step += 1
        self.state_history.append(state)
        self.prediction_history.append(prediction[0])
        self.action_history.append(re.Action(np.argmax(solution)))
        return self.action_history[-1]



    def learn(self, reward):

        action_pool_len = len(self.actions_pool)
        batch_size = len(self.action_history)

        assert batch_size > 0, "no history found"

        divided_reward = reward/24
        predictions = self.prediction_history
        for i in range(batch_size-2):
            x = predictions[i][self.action_history[i].id]
            x = divided_reward + gamma*max(predictions[i+1])
            predictions[i][self.action_history[i].id] = x
        predictions[batch_size-1][self.action_history[batch_size-1].id] = divided_reward

        states = np.array(self.state_history, float)
        learned_predict = np.array(predictions, float)
        #if reward > enough_reward:
        #    actions = [i.id for i in self.action_history]               # агент действовал правильно
        #else:
        #    actions = [random.randint(0, action_pool_len -1) for i in range(batch_size)] # нужно действовать иначе
        #actions = np_utils.to_categorical(actions, action_pool_len)

        learned_predict = np.array(predictions, float)
        #print(states
        with graph.as_default():
            self.solutor.fit(states,                                    # история состояний
                             learned_predict,                            # "правильные" действия
                             batch_size = batch_size,
                             verbose = 0)

        self.state_history = []
        self.action_history = []
        self.prediction_history = []



    def save(self):
        file = open("solutor_wights.json", 'w')
        file.write(self.solutor.to_json())
        file.close()
        self.solutor.save_weights("solutor_weights.h5")


    def load(self):
        json_file = open("solutor_weights.json", 'r')
        self.solutor = model_from_json(json_file.read())
        json_file.close()
        self.solutor.load_weights("solutor_weights.h5")
        self.solutor.compile(loss = "categorical_crossentropy",
                             optimizer='SGD',
                             metrics=['accuracy'])