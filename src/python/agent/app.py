# to run: $ env FLASK_APP=app.py flask run
# flask run --host=0.0.0.0

from flask import Flask, jsonify, make_response, request
from rl_entity import State, Action
from agent import Agent

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello, World! This is the First python server!"

if __name__ == '__main__':
    app.run(host = '0.0.0.0')

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol', 
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web', 
        'done': False
    }
]

@app.route('/state', methods=['GET'])
def get_state_ex():
    return jsonify(State().serialize())


@app.route('/action/<int:id>', methods=['GET'])
def get_action_ex(id):
    return jsonify(Action(id).serialize())



agent = None
@app.route('/agent/init', methods=['PUT'])
def init_agent():
    global agent
    #if agent is None:
    state = State()
    state.price_delta_last_hour = [[0.4]*4]*60
    action_size = len(Action(3)._values)
    agent = Agent(state, [Action(i) for i in range(action_size)])
    return jsonify({"status": "created"})
    #return make_response(jsonify({'status': 'not created'}), 400)



@app.route('/agent/reset', methods=['PUT'])
def reset_agent():
    global agent
    if agent is None:
        return make_response(jsonify({'status': 'not created'}), 400)
    state = State()
    state.price_delta_last_hour = [[0]*4]*60

    action_size = len(Action(3)._values)
    agent = Agent(state, [Action(i) for i in range(action_size)])
    return jsonify({"status": "reseted"})



@app.route('/agent/decide', methods=['POST'])
def decide():
    global agent
    if agent is None:
        return make_response(jsonify({'status': 'agent not created'}), 400)
    state = State()
    state.price_delta_last_hour = request.json['price_delta_last_hour']
    state.coin_part = request.json['coin_part']
    state.coin_hour_delta = request.json['coin_hour_delta']
    state.portfolio_hour_delta = request.json['portfolio_hour_delta']
    state.coin_d_delta = request.json['coin_d_delta']
    state.coin_w_delta = request.json['coin_w_delta']
    state.coin_m_delta = request.json['coin_m_delta']
    state.coin_3m_delta = request.json['coin_3m_delta']
    state.coin_6m_delta = request.json['coin_6m_delta']
    state.coin_y_delta = request.json['coin_y_delta']
    state.portfolio_d_delta = request.json['portfolio_d_delta']
    state.portfolio_w_delta = request.json['portfolio_w_delta']
    state.portfolio_m_delta = request.json['portfolio_m_delta']
    state.deviation_exp = request.json['deviation_exp']
    state.deviation_line = request.json['deviation_line']
    state.volatility_exp = request.json['volatility_exp']
    state.volatility_line = request.json['volatility_line']
    state.m_drowdown = request.json['m_drowdown']

    return jsonify(agent.decide(state).serialize())


@app.route('/agent/learn', methods=['POST'])
def agent_learn():
    global agent
    if agent is None:
        return make_response(jsonify({'status': 'no agent here'}), 400)
    agent.learn(float(request.json['reward']))
    return jsonify({"status": "learned"})


@app.route('/agent/save', methods=['GET'])
def save_agent():
    global agent
    if agent is None:
        return make_response(jsonify({'status': 'no agent here'}), 400)
    agent.save()
    return jsonify({"status": "saved"})
                                      
    
@app.route('/agent/load', methods=['GET'])
def load_agent():
    global agent
    if agent is None:
        return make_response(jsonify({'status': 'no agent here'}), 400)
    agent.load()
    return jsonify({"status": "loaded"})
                                     
                                      
# error handlers

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(500)
def not_found(error):
    return make_response(jsonify({'error': 'Internal error :('}), 500)
