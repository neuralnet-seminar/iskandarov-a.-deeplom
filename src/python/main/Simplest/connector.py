import csv
from decimal import Decimal

def csv_reader(file_obj):
    """
    Read csv file
    """
    reader = csv.DictReader(file_obj,delimiter=';')
    a = []
    for line in reader:
        a.append(line)
    return reader.fieldnames, a

def head_and_market():
    path = "market_data/GDAX.BTC-USD_150101_190923.csv"
    with open(path, "r") as f_obj:
        h, m = csv_reader(f_obj)
    for mm in m:
        for i in range(4, len(h)):
            mm[h[i]] = Decimal(mm[h[i]])
    return h, m

commission = Decimal(0.0025) #0.25% от объёма сделки

class SimulatorConnector(object):
    
    def __init__(self):
        self.__step = 0
        self._btc_headers, self._btc_market = head_and_market()
        self._cash = Decimal(100)
        self._coins = Decimal(0)
    
    def get_init_state(self):
        self.__step = 0
        return self._btc_market[0]

    def get_next_state(self):
        if self.__step < len(self._btc_market) -1 :
            self.__step += 1
        return self._btc_market[self.__step]
    
    def get_headers(self):
        return self._btc_headers
    
    def get_balance(self):
        total_value = self._cash + self._coins * self._btc_market[self.__step]["<CLOSE>"]
        return self._cash, self._coins, total_value
    
    def buy(self, amount): #amount = coin volume
        amount = Decimal(amount)
        price = self._btc_market[self.__step]["<CLOSE>"]
        money_need = price * amount * (commission + 1)
        if self._cash >= money_need:
            self._coins += amount
            self._cash -= money_need
            return True
        return False
    
    def sell(self, amount): #amount = coin volume
        amount = Decimal(amount)
        price = self._btc_market[self.__step]["<CLOSE>"]
        if self._coins >= amount:
            self._coins -= amount
            self._cash += (-commission + 1) * price * amount
            return True
        return False
    