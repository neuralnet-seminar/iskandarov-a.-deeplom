# Агенту будет доступно 20 действий, 0 <= id < 20. 
# "buy 25%" означает увеличить долю актива в портфеле на 25 п.п. 
#     (пример: 10% -> 35%, 90% -> 100%)
# "sell 50%" - снизить долю актива в портфеле на 50 п.п. 
#     (пример: 80% -> 30%, 45% -> 0%)

from decimal import Decimal

class Action(object):
    
    __actions = ["buy 50%", "buy 25%", "buy 15%", "buy 10%", "buy 7%", "buy 5%", 
                 "buy 3%", "buy 2%", "buy 1%", "wait", 
                 "sell 1%", "sell 2%", "sell 3%", "sell 5%", "sell 7%", "sell 10%",
                 "sell 15%", "sell 25%", "sell 50%", "sell 100%"]
    
    __values = [50, 25, 15, 10, 7, 5, 3, 2, 1, 0, -1, -2, -3, -5, -7, -10, -15, -25, -50, -100]
    
    def __init__(self, x):
        self.id = x
    
    def name(self):
        return self.__actions[self.id]
    
    def value(self):
        return Decimal(self.__values[self.id])/100

    
    
class State(object):
    
    def __init__(self):
        # cash ratio
        self.cash = Decimal(1)
        # coins ratio
        self.assets = Decimal(0)
        # delta of total portfolio value
        self.value_delta = Decimal(0)
        # delta of coin price
        self.price_delta = Decimal(0)
        # day delta of coin price
        self.day_price_delta = Decimal(0)
    
    def __repr__(self):
        return "State (cash: %.2f, assets: %.2f, value_delta: %.2f, price_delta: %.2f, day_pr_delta: %.2f)" % (
            self.cash, 
            self.assets,
            self.value_delta,
            self.price_delta,
            self.day_price_delta)
    
    def to_array(self):
        return [self.cash, self.assets, self.value_delta, self.price_delta, self.day_price_delta]
