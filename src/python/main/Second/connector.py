import csv
from decimal import Decimal

def csv_reader(file_obj):
    """
    Read csv file
    """
    reader = csv.DictReader(file_obj,delimiter=',')
    a = []
    for line in reader:
        a.append(line)
    return reader.fieldnames, a

def head_and_market(path):
    with open(path, "r") as f_obj:
        h, m = csv_reader(f_obj)
    for mm in m:
        for i in range(len(h)):
            mm[h[i]] = mm[h[i]]
    return h, m

commission = 0.0025 #0.25% от объёма сделки

class SimulatorConnector(object):
    
    def __init__(self, coin_pair):
        path = {"BTC-USD":"market_data/btc-usd-1min.csv",
                "ETH-USD":"market_data/eth-usd-1min.csv"}.get(coin_pair)
        self._step = 0
        self._btc_headers, self._btc_market = head_and_market(path)
        self.cash = 100
        self.coins = 0
    
    def get_init_state(self):
        self._step = 0
        return self._btc_market[0]

    def get_next_state(self):
        if self._step < len(self._btc_market) -1 :
            self._step += 1
        return self._btc_market[self._step]
    
    def get_headers(self):
        return self._btc_headers
    
    def get_balance(self):
        total_value = round(self.cash + self.coins * self._btc_market[self._step]["<CLOSE>"],2)
        return self.cash, self.coins, total_value
    
    def buy(self, amount): # amount = coin count
        price = self._btc_market[self._step]["<CLOSE>"]
        money_need = price * amount * (commission + 1)
        if self.cash >= money_need:
            self.coins += amount
            self.cash -= money_need
            return True
        return False
    
    def sell(self, amount): # amount = coin count
        price = self._btc_market[self._step]["<CLOSE>"]
        if self._coins >= amount:
            self._coins -= amount
            self._cash += (-commission + 1) * price * amount
            return True
        return False
    