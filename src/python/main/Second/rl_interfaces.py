# You must provide __init__ method, that creates neural net.
# Maybe, you`ll want to know input and ouput dimentions

from keras.models import Sequential, model_from_json
from keras.layers import Dense
from keras.utils import np_utils
from keras.callbacks import History

import random
random.seed(42)


agent_batch_size = 10

class FirstAgent(object):
    
    def __init__(self):
        self.step = 0
        self.env = FirstEnvironment()
        self.state_history = []
        self.reward_hisotry = []
        self.action_history = []
        self.state_history.append(self.env.get_init_state())
        self.actions = self.env.get_available_actions()
        self.__init_newral_net(self.state_history[-1], actions)
        self.env.get_reward = mock_reward
        
    def __init_newral_net(self, state_exapmle, actions):
        solutor = Sequential()
        solutor.add(Dense(3,                           # количество нейронов на первом слое (3 = наугад)
                            kernel_initializer="normal", # норм. распр. начальных весов
                            input_dim = len(state_exapmle.to_array()), 
                            activation="relu"))          # функция активации: max(0,x)
        solutor.add(Dense(len(actions),                # кол-во выходных нейронов
                            kernel_initializer="normal", 
                            activation="sigmoid"))       # сигмоидная ф-я активации
        solutor.compile(loss = "categorical_crossentropy", # т.к. сеть классифицирует
                        optimizer='SGD',               # стохаст. град. спуск
                        metrics=['accuracy'])          # точность
        self.solutor = solutor
    
    
    def one_step(self):
        state = self.env.get_state()
        reward = self.env.get_reward()
        self.reward_hisotry.append(Decimal(reward))
        self.state_history.append(state.to_array())
        self.learn()
        action = self.decide()
        return self.env.do_action(action)
        
        
        
    def learn(self):
        if self.step > 0 and self.step % agent_batch_size == 0:
            states = self.state_history[-agent_batch_size-1:-1]
            total_reward = sum(self.reward_hisotry[-agent_batch_size:])
            if total_reward > 0:
                actions = self.action_history[-agent_batch_size:]             # агент действовал правильно
            else:
                actions = [random.randint(0, 19) for i in range(20)]          # нужно действовать иначе
            actions = np_utils.to_categorical([actions], len(self._actions))
            
            self.solutor.fit(states,                                          # история состояний
                             actions,                                         # "правильные" действия
                             batch_size=agent_batch_size, 
                             verbose=0)
    
    
    def save_solutor(self):
        file = open("solutor_wights.json", 'w')
        file.write(self.solutor.model_to_json)
        file.close()
        self.solutor.save_weights("solutor_weights.h5")
            
    def load_solutor(self):
        json_file = open("solutor_weights.json", 'r')
        self.solutor = model_from_json(json_file.read())
        json_file.close()
        self.solutor.load_weights("solutor_weights.h5")
        self.solutor.compile(loss = "categorical_crossentropy",
                           optimizer='SGD',
                           metrics=['accuracy'])
        

        
        
        
        
        
        
# Пришла идея активным сделать агента, а не среду
from connector import SimulatorConnector
from decimal import Decimal
import rl_entity as re
import statistics

for_commission = Decimal(0.01) # среда будет оставлять наличных, чтобы хватило на комиссию

class FirstEnvironment(object):
    
    def __init__(self):
        self.__connector = SimulatorConnector()
        self.__vector = None
        self.__total = Decimal(0)
        self.__state_history = []
        self.__reward = 0
    
    
    def get_init_state(self):
        self.__vector = self.__connector.get_init_state()
        self.__cash, self.__coins, self.__total_old = self.__connector.get_balance()
        state = re.State()
        self.__state_history.append(state)
        return state.to_array()
    
    
    def get_available_actions(self):
        return [re.Action(i) for i in range(20)]
    
    def get_reward(self):
        return self.__reward
    
    def get_state(self):
        if self.__vector is None:
            raise Exception("Did not called get_init_state()")
        vector = self.__connector.get_next_state()
        self.__cash, self.__coins, self.__total = self.__connector.get_balance()
        return self._calc_state(vector).to_array()
    
    
    def _calc_state(self, vector):
        cash, coins, total = self.__cash, self.__coins, self.__total
        self.__reward = total - self.__total_old
        price = vector["<CLOSE>"]
        state = re.State()
        state.cash = cash/(cash + coins * price)
        state.assets = coins * price/(cash+coins * price)
        state.value_delta = total/self.__total_old - 1
        state.price_delta = vector["<CLOSE>"]/self.__vector["<CLOSE>"] - 1
        state.day_price_delta = vector["<CLOSE>"]/vector["<OPEN>"] -1
        self.append_state(state)
        self.__vector = vector
        self.__total_old = total
        return state
    
    
    def append_state(self, state):       # храним 10(?) последних состояний
        sh = self.__state_history
        sh.append(state)
        sh = sh[max(0, len(sh)-10):]
    
    
    def do_action(self, action):
        assert action.id < 20, "action id must be < 20"
        if action.id == 9:
            return True
        if action.id in range(0, 9): # buy
            return self.__connector.buy(self._calc_buy_amount(action))
        else:                         # sell
            return self.__connector.sell(self._calc_sell_amount(action))
        
        
    def _calc_buy_amount(self, action):
        ratio_bound = min(action.value(), 1-self.__state_history[-1].assets) * (1-for_commission) # = доля от total
        money_need = ratio_bound * self.__total
        return money_need / self.__vector["<CLOSE>"]
    
    
    def _calc_sell_amount(self, action):
        ratio_bound = min(-action.value(), self.__state_history[-1].assets)
        money_need = ratio_bound * self.__total
        return money_need / self.__vector["<CLOSE>"]
    
    
         